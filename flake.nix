{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    poetry2nix = {
      url = "github:nix-community/poetry2nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    futils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, poetry2nix, futils } @ inputs:
    let
      inherit (nixpkgs) lib;
      inherit (lib) recursiveUpdate;
      inherit (futils.lib) eachDefaultSystem defaultSystems;

      nixpkgsFor = lib.genAttrs defaultSystems (system: import nixpkgs {
        inherit system;
        config.allowUnfree  = true;
        overlays = [ poetry2nix.overlays.default ];
      });
    in
    (eachDefaultSystem (system:
      let
        pkgs = nixpkgsFor.${system};
      in
      {
        devShell = pkgs.mkShell {
          buildInputs = with pkgs; [
            (pkgs.poetry2nix.mkPoetryEnv {
              projectDir = self;

              overrides = [
                pkgs.poetry2nix.defaultPoetryOverrides
                (self: super: {
                  hvac = super.hvac.overridePythonAttrs (old: {
                    buildInputs = old.buildInputs ++ [ self.poetry ];
                  });

                  click = super.click.overridePythonAttrs (old: {
                    buildInputs = old.buildInputs ++ [ self.flit-core ];
                  });

                  rfc3986-validator = super.rfc3986-validator.overridePythonAttrs (old: {
                    buildInputs = old.buildInputs ++ [ self.setuptools self.pytest-runner ];
                  });

                  # https://github.com/nix-community/poetry2nix/issues/1807
                  "urllib3" = super."urllib3".overridePythonAttrs (old: {
                    buildInputs = (old.buildInputs or [ ]) ++ [ super.hatch-vcs ];
                  });

                  rpds-py = super.rpds-py.overridePythonAttrs (old: {
                    cargoDeps = pkgs.rustPlatform.fetchCargoTarball {
                      inherit (old) src;
                      name = "${old.pname}-${old.version}";
                      sha256 = "sha256-m01OB4CqDowlTAiDQx6tJ7SeP3t+EtS9UZ7Jad6Ccvc=";
                    };
                  });
                })
              ];
            })
            go
            git
            jq
            jsonnet
            jsonnet-bundler
            mdbook
            mdbook-linkcheck
            mdbook-mermaid
            openssl
            poetry
            pre-commit
            shellcheck
            opentofu
            vault
          ];

          shellHook = ''
            source ./config.sh
          '';

          # See https://github.com/NixOS/nix/issues/318#issuecomment-52986702
          LOCALE_ARCHIVE = "${pkgs.glibcLocales}/lib/locale/locale-archive";
        };
      }
    ));
}
