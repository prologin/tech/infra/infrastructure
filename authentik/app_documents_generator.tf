locals {
  documents_generator_allowed_groups = [
    "staff",
  ]
  documents_superuser_groups = [
    "presidence",
    "bureau",
  ]
  documents_generator_allowed_users = [
    authentik_user.ndf.id,
  ]
}

resource "random_string" "documents-generator-oidc" {
  count   = 2
  length  = 64
  special = false
}

resource "authentik_property_mapping_provider_scope" "documents_roles" {
  name       = "documents Roles"
  scope_name = "roles"
  expression = <<EOF
roles = []

%{for group in local.documents_superuser_groups}
if ak_is_group_member(request.user, name="${group}"):
    roles.append("superuser")
%{endfor}

return {
  "roles": list(set(roles)),
}
EOF
}

resource "authentik_provider_oauth2" "documents-generator-oidc" {
  name               = "documents-generator-oidc"
  authorization_flow = data.authentik_flow.default-provider-authorization-implicit-consent.id
  invalidation_flow = data.authentik_flow.default-provider-invalidation-flow.id

  client_id          = random_string.documents-generator-oidc[0].result
  client_secret      = random_string.documents-generator-oidc[1].result

  access_code_validity  = "hours=1"
  access_token_validity = "days=30"

  sub_mode = "user_email"

  allowed_redirect_uris = [
    {
      matching_mode = "strict",
      url = "https://documents.prologin.org/rest/auth/oidc/callback/",
    }
  ]

  property_mappings = [
    data.authentik_property_mapping_provider_scope.scope-email.id,
    data.authentik_property_mapping_provider_scope.scope-openid.id,
    data.authentik_property_mapping_provider_scope.scope-profile.id,
    authentik_property_mapping_provider_scope.documents_roles.id,
  ]

  signing_key = data.authentik_certificate_key_pair.default.id
}

resource "authentik_application" "documents-generator-oidc" {
  name               = "Documents"
  slug               = "documents-generator"
  group              = "Staff"
  protocol_provider  = authentik_provider_oauth2.documents-generator-oidc.id
  policy_engine_mode = "any"

  meta_launch_url  = "https://documents.prologin.org"
  meta_icon        = "https://cdn.jsdelivr.net/gh/walkxcode/dashboard-icons/png/archivebox.png"
  meta_description = "Génération automatisée des documents de l'association"
}

resource "authentik_policy_binding" "documents-generator_group-filtering" {
  for_each = { for idx, value in local.documents_generator_allowed_groups : idx => value }

  target = authentik_application.documents-generator-oidc.uuid
  group  = data.authentik_group.groups[each.value].id
  order  = each.key
}

resource "authentik_policy_binding" "documents-generator_user-filtering" {
  for_each = { for idx, value in local.documents_generator_allowed_users : idx => value }

  target = authentik_application.documents-generator-oidc.uuid
  user   = each.value
  order  = each.key
}

resource "vault_generic_secret" "authentik_apps_documents-generator_oidc" {
  path = "authentik/apps/documents-generator/oidc"
  data_json = jsonencode({
    client_id = authentik_provider_oauth2.documents-generator-oidc.client_id
    secret_id = authentik_provider_oauth2.documents-generator-oidc.client_secret
  })
}
