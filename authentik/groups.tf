data "authentik_group" "groups" {
  for_each = toset([
    "bureau",
    "presidence",
    "respo-concours",
    "respo-est",
    "respo-idf",
    "respo-nord-ouest",
    "respo-ouest",
    "respo-pole",
    "respo-regionaux",
    "respo-sud",
    "respo-tech",
    "respos",
    "roots",
    "secretariat",
    "service-accounts",
    "staff",
    "tresorerie",
    "vice-presidence",

    "pie",

    "authentik Admins",
  ])
  name = each.key
}
