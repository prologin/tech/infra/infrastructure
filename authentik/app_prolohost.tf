locals {
  prolohost_allowed_groups = [
    "staff",
  ]
  prolohost_superuser_groups = [
    "roots",
  ]
  prolohost_staff_groups = [
  ]
}


resource "random_string" "prolohost-oidc" {
  count   = 2
  length  = 64
  special = false
}

resource "authentik_property_mapping_provider_scope" "prolohost_roles" {
  name       = "prolohost Roles"
  scope_name = "roles"
  expression = <<EOF
roles = []

%{for group in local.prolohost_superuser_groups}
if ak_is_group_member(request.user, name="${group}"):
    roles.append("superuser")
%{endfor}

%{for group in local.prolohost_staff_groups}
if ak_is_group_member(request.user, name="${group}"):
    roles.append("staff")
%{endfor}

return {
  "roles": list(set(roles)),
  "groups": [g.name for g in request.user.ak_groups.all()],
}
EOF
}

resource "authentik_provider_oauth2" "prolohost-oidc" {
  name               = "prolohost-oidc"
  authorization_flow = data.authentik_flow.default-provider-authorization-implicit-consent.id
  invalidation_flow = data.authentik_flow.default-provider-invalidation-flow.id

  client_id          = random_string.prolohost-oidc[0].result
  client_secret      = random_string.prolohost-oidc[1].result

  access_code_validity  = "hours=1"
  access_token_validity = "days=1"

  sub_mode = "user_email"

  allowed_redirect_uris = [
    {
      matching_mode = "strict",
      url           = "https://demi-finale.prologin.org:8443/complete/oidc/",
    }
  ]

  property_mappings = [
    data.authentik_property_mapping_provider_scope.scope-email.id,
    data.authentik_property_mapping_provider_scope.scope-openid.id,
    data.authentik_property_mapping_provider_scope.scope-profile.id,
    authentik_property_mapping_provider_scope.prolohost_roles.id,
  ]

  signing_key = data.authentik_certificate_key_pair.default.id
}

resource "authentik_application" "prolohost-oidc" {
  name               = "Prolohost"
  slug               = "prolohost"
  group              = "Prologin"
  protocol_provider  = authentik_provider_oauth2.prolohost-oidc.id
  policy_engine_mode = "any"

  meta_launch_url  = "https://demi-finale.prologin.org:8443"
  meta_icon        = "https://www.silicon.fr/wp-content/uploads/2013/06/Dedibox-SC.png"
  meta_description = "Console d'hébergement de demi-finale"
}

resource "authentik_policy_binding" "prolohost_group-filtering" {
  for_each = { for idx, value in local.prolohost_allowed_groups : idx => value }

  target = authentik_application.prolohost-oidc.uuid
  group  = data.authentik_group.groups[each.value].id
  order  = each.key
}

resource "vault_generic_secret" "authentik_apps_prolohost_oidc" {
  path = "authentik/apps/prolohost/oidc"
  data_json = jsonencode({
    client_id = authentik_provider_oauth2.prolohost-oidc.client_id
    secret_id = authentik_provider_oauth2.prolohost-oidc.client_secret
  })
}


resource "authentik_user" "prolohost" {
  username = "prolohost"
  email    = "prolohost@prologin.org"
  name     = "prolohost"
  path     = "users/prologin/service_accounts"
  groups   = [data.authentik_group.groups["authentik Admins"].id]
}

resource "authentik_token" "prolohost" {
  identifier = "prolohost"
  user       = authentik_user.prolohost.id
  intent     = "api"
  expiring   = false

  retrieve_key = true
}

resource "vault_generic_secret" "authentik_apps_prolohost_user" {
  path = "authentik/apps/prolohost/user"
  data_json = jsonencode({
    token = authentik_token.prolohost.key
  })
}
