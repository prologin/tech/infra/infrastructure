locals {
  wikijs_allowed_groups = [
    "staff",
  ]
}

resource "random_string" "wikijs-oidc" {
  count   = 2
  length  = 64
  special = false
}

resource "authentik_provider_oauth2" "wikijs-oidc" {
  name               = "wikijs-oidc"
  authorization_flow = data.authentik_flow.default-provider-authorization-implicit-consent.id
  invalidation_flow = data.authentik_flow.default-provider-invalidation-flow.id
  client_id          = random_string.wikijs-oidc[0].result
  client_secret      = random_string.wikijs-oidc[1].result

  access_code_validity  = "hours=1"
  access_token_validity = "days=30"

  sub_mode = "user_username"

  allowed_redirect_uris = [
    {
      matching_mode = "strict",
      url           = "https://wiki.prologin.org/login/3d4ab205-14ca-4277-a628-19ec92ab1eaa/callback",
    }
  ]

  property_mappings = [
    data.authentik_property_mapping_provider_scope.scope-email.id,
    data.authentik_property_mapping_provider_scope.scope-openid.id,
    data.authentik_property_mapping_provider_scope.scope-profile.id,
  ]

  signing_key = data.authentik_certificate_key_pair.default.id
}

resource "authentik_application" "wikijs-oidc" {
  name               = "Wiki.js"
  slug               = "wikijs"
  group              = "Staff"
  protocol_provider  = authentik_provider_oauth2.wikijs-oidc.id
  policy_engine_mode = "any"

  meta_launch_url  = "https://wiki.prologin.org"
  meta_icon        = "https://cdn.jsdelivr.net/gh/walkxcode/dashboard-icons/png/wikijs.png"
  meta_description = "Banque de connaissance de l'association"
}

resource "authentik_policy_binding" "wikijs_group-filtering" {
  for_each = { for idx, value in local.wikijs_allowed_groups : idx => value }

  target = authentik_application.wikijs-oidc.uuid
  group  = data.authentik_group.groups[each.value].id
  order  = each.key
}

resource "vault_generic_secret" "authentik_apps_wikijs_oidc" {
  path = "authentik/apps/wikijs/oidc"
  data_json = jsonencode({
    client_id = authentik_provider_oauth2.wikijs-oidc.client_id
    secret_id = authentik_provider_oauth2.wikijs-oidc.client_secret
  })
}
