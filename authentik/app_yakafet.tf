locals {
  yakafet_allowed_groups = [
    "pie",
    "staff",
  ]
  yakafet_superuser_groups = [
    "bureau",
    "roots",
  ]
  yakafet_staff_groups = [
    "staff",
  ]
}


resource "random_string" "yakafet-oidc" {
  count   = 2
  length  = 64
  special = false
}

resource "authentik_property_mapping_provider_scope" "yakafet_roles" {
  name       = "yakafet Roles"
  scope_name = "roles"
  expression = <<EOF
roles = []

%{for group in local.yakafet_superuser_groups}
if ak_is_group_member(request.user, name="${group}"):
    roles.append("superuser")
%{endfor}

%{for group in local.yakafet_staff_groups}
if ak_is_group_member(request.user, name="${group}"):
    roles.append("staff")
%{endfor}

return {
  "roles": list(set(roles)),
}
EOF
}

resource "authentik_provider_oauth2" "yakafet-oidc" {
  name               = "yakafet-oidc"
  authorization_flow = data.authentik_flow.default-provider-authorization-implicit-consent.id
  invalidation_flow = data.authentik_flow.default-provider-invalidation-flow.id

  client_id          = random_string.yakafet-oidc[0].result
  client_secret      = random_string.yakafet-oidc[1].result

  access_code_validity  = "hours=1"
  access_token_validity = "days=1"

  sub_mode = "user_email"

  allowed_redirect_uris = [
    {
      matching_mode = "strict",
      url           = "https://yakafet.girlscancode.fr/rest/auth/oidc/callback/",
    }
  ]

  property_mappings = [
    data.authentik_property_mapping_provider_scope.scope-email.id,
    data.authentik_property_mapping_provider_scope.scope-openid.id,
    data.authentik_property_mapping_provider_scope.scope-profile.id,
    authentik_property_mapping_provider_scope.yakafet_roles.id,
  ]

  signing_key = data.authentik_certificate_key_pair.default.id
}

resource "authentik_application" "yakafet-oidc" {
  name               = "YakaFet"
  slug               = "yakafet"
  group              = "Girls Can Code!"
  protocol_provider  = authentik_provider_oauth2.yakafet-oidc.id
  policy_engine_mode = "any"

  meta_launch_url  = "https://yakafet.girlscancode.fr"
  meta_icon        = "https://cdn.jsdelivr.net/gh/walkxcode/dashboard-icons/png/radicale.png"
  meta_description = "Qu'est-ce qu'on mange ?"
}

resource "authentik_policy_binding" "yakafet_group-filtering" {
  for_each = { for idx, value in local.yakafet_allowed_groups : idx => value }

  target = authentik_application.yakafet-oidc.uuid
  group  = data.authentik_group.groups[each.value].id
  order  = each.key
}

resource "vault_generic_secret" "authentik_apps_yakafet_oidc" {
  path = "authentik/apps/yakafet/oidc"
  data_json = jsonencode({
    client_id = authentik_provider_oauth2.yakafet-oidc.client_id
    secret_id = authentik_provider_oauth2.yakafet-oidc.client_secret
  })
}


resource "authentik_user" "yakafet" {
  username = "yakafet"
  email    = "yakafet@prologin.org"
  name     = "yakafet"
  path     = "users/prologin/service_accounts"
  groups   = [data.authentik_group.groups["authentik Admins"].id]
}

resource "authentik_token" "yakafet" {
  identifier = "yakafet"
  user       = authentik_user.yakafet.id
  intent     = "api"
  expiring   = false

  retrieve_key = true
}

resource "vault_generic_secret" "authentik_apps_yakafet_user" {
  path = "authentik/apps/yakafet/user"
  data_json = jsonencode({
    token = authentik_token.yakafet.key
  })
}
