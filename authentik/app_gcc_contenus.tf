resource "authentik_application" "gcc-contenus" {
  name  = "TPs Girls Can Code!"
  slug  = "gcc-contenus"
  group = "Girls Can Code!"

  meta_launch_url  = "https://tp.girlscancode.fr"
  meta_icon        = "https://tp.girlscancode.fr/favicon.svg"
  meta_description = "Contenus des stages Girls Can Code!"
}
