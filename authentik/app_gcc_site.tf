locals {
  gcc_site_allowed_groups = [
    "staff",
  ]
}

resource "random_string" "gcc-site-oidc" {
  count   = 2
  length  = 64
  special = false
}

resource "authentik_property_mapping_provider_scope" "gcc-site_roles" {
  name       = "GCC site Roles"
  scope_name = "roles"
  expression = <<EOF
role_mapping = {
  "access": ["staff"],
  "staff": ["staff"],
  "superuser": ["roots", "presidence"],
}

return {
  "roles": [
    role_name
    for role_name, role_groups in role_mapping.items()
    if any(ak_is_group_member(request.user, name=group_name) for group_name in role_groups)
  ]
}
EOF
}

resource "authentik_provider_oauth2" "gcc-site-oidc" {
  name               = "gcc-site-oidc"
  authorization_flow = data.authentik_flow.default-provider-authorization-explicit-consent.id
  invalidation_flow = data.authentik_flow.default-provider-invalidation-flow.id
  client_id          = random_string.gcc-site-oidc[0].result
  client_secret      = random_string.gcc-site-oidc[1].result

  access_code_validity  = "hours=1"
  access_token_validity = "days=1"

  sub_mode = "user_email"

  allowed_redirect_uris = [
    {
      matching_mode = "strict",
      url           = "https://girlscancode.fr/complete/oidc/",
    }
  ]

  property_mappings = [
    data.authentik_property_mapping_provider_scope.scope-email.id,
    data.authentik_property_mapping_provider_scope.scope-openid.id,
    data.authentik_property_mapping_provider_scope.scope-profile.id,
    authentik_property_mapping_provider_scope.gcc-site_roles.id,
  ]

  signing_key = data.authentik_certificate_key_pair.default.id
}

resource "authentik_application" "gcc-site-oidc" {
  name               = "Girls Can Code!"
  slug               = "gcc-site"
  group              = "Staff"
  protocol_provider  = authentik_provider_oauth2.gcc-site-oidc.id
  policy_engine_mode = "any"

  meta_launch_url  = "https://girlscancode.fr"
  meta_icon        = "https://girlscancode.fr/static/favicon.svg"
  meta_description = "Site Girls Can Code!"
}

resource "authentik_policy_binding" "gcc_site_allowed_groups-filtering" {
  for_each = { for idx, value in local.gcc_site_allowed_groups : idx => value }

  target = authentik_application.gcc-site-oidc.uuid
  group  = data.authentik_group.groups[each.value].id
  order  = each.key
}

resource "vault_generic_secret" "authentik_apps_gcc-site-oidc" {
  path = "authentik/apps/gcc-site/oidc"
  data_json = jsonencode({
    client_id = authentik_provider_oauth2.gcc-site-oidc.client_id
    secret_id = authentik_provider_oauth2.gcc-site-oidc.client_secret
  })
}
