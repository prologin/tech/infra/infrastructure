locals {
  grafana-org_allowed_groups = [
    "roots",
    "bureau",
    "respo-concours",
  ]
}

resource "random_string" "grafana-org-oidc" {
  count   = 2
  length  = 64
  special = false
}

resource "authentik_provider_oauth2" "grafana-org-oidc" {
  name               = "grafana-org-oidc"
  authorization_flow = data.authentik_flow.default-provider-authorization-implicit-consent.id
  invalidation_flow = data.authentik_flow.default-provider-invalidation-flow.id
  client_id          = random_string.grafana-org-oidc[0].result
  client_secret      = random_string.grafana-org-oidc[1].result

  access_code_validity  = "hours=1"
  access_token_validity = "days=30"

  sub_mode = "user_email"
  allowed_redirect_uris = [
    {
      matching_mode = "strict",
      url           = "https://grafana.prologin.org/login/generic_oauth",
    }
  ]

  property_mappings = [
    data.authentik_property_mapping_provider_scope.scope-email.id,
    data.authentik_property_mapping_provider_scope.scope-openid.id,
    data.authentik_property_mapping_provider_scope.scope-profile.id,
  ]

  signing_key = data.authentik_certificate_key_pair.default.id
}

resource "authentik_application" "grafana-org-oidc" {
  name               = "grafana-org"
  slug               = "grafana-org"
  group              = "Infra"
  protocol_provider  = authentik_provider_oauth2.grafana-org-oidc.id
  policy_engine_mode = "any"

  meta_launch_url  = "https://grafana.prologin.org"
  meta_icon        = "https://cdn.jsdelivr.net/gh/walkxcode/dashboard-icons/png/grafana.png"
  meta_description = "Observability Dashboards"
}

resource "authentik_policy_binding" "grafana-org_group-filtering" {
  for_each = { for idx, value in local.grafana-org_allowed_groups : idx => value }

  target = authentik_application.grafana-org-oidc.uuid
  group  = data.authentik_group.groups[each.value].id
  order  = each.key
}

resource "vault_generic_secret" "authentik_apps_grafana-org_oidc" {
  path = "authentik/apps/grafana-org/oidc"
  data_json = jsonencode({
    client_id = authentik_provider_oauth2.grafana-org-oidc.client_id
    secret_id = authentik_provider_oauth2.grafana-org-oidc.client_secret
  })
}
