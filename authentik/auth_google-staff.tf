data "vault_generic_secret" "authentik_auth-providers_google-staff" {
  path = "authentik/auth-providers/google-staff"
}

resource "authentik_source_oauth" "google-staff" {
  name          = "Google Staff"
  provider_type = "google"
  slug          = "google-staff"

  authentication_flow = data.authentik_flow.default-source-authentication.id
  enrollment_flow     = data.authentik_flow.default-source-enrollment.id
  user_matching_mode  = "email_link"

  consumer_key    = data.vault_generic_secret.authentik_auth-providers_google-staff.data["client_id"]
  consumer_secret = data.vault_generic_secret.authentik_auth-providers_google-staff.data["secret_id"]
}
