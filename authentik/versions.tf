terraform {
  required_providers {
    authentik = {
      source  = "goauthentik/authentik"
      version = "2024.10.2"
    }
    random = {
      source  = "hashicorp/random"
      version = "3.4.3"
    }
    vault = {
      source  = "hashicorp/vault"
      version = "3.12.0"
    }
  }

  backend "http" {}
}

provider "vault" {}

data "vault_generic_secret" "authentik_admin" {
  path = "authentik/admin"
}

provider "authentik" {
  url   = "https://auth.prologin.org"
  token = data.vault_generic_secret.authentik_admin.data["token"]
}
