locals {
  google_drive_allowed_groups = [
    "roots",
  ]
}

resource "authentik_application" "google-drive" {
  name               = "Google Drive"
  slug               = "google-drive"
  group              = "Staff"
  policy_engine_mode = "any"

  meta_launch_url  = "https://drive.prologin.org"
  meta_icon        = "https://cdn.jsdelivr.net/gh/walkxcode/dashboard-icons/png/google-drive.png"
  meta_description = "Fichiers internes de l'association"
}

resource "authentik_policy_binding" "google_drive_group-filtering" {
  for_each = { for idx, value in local.google_drive_allowed_groups : idx => value }

  target = authentik_application.google-drive.uuid
  group  = data.authentik_group.groups[each.value].id
  order  = each.key
}
