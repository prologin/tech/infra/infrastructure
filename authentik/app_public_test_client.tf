locals {
  public_test_client_allowed_groups = [
    "staff",
  ]
}

resource "random_string" "public-test-client-oidc" {
  count   = 2
  length  = 64
  special = false
}

resource "authentik_property_mapping_provider_scope" "public-test-client_roles" {
  name       = "Public test client Roles"
  scope_name = "roles"
  expression = <<EOF
return {
  "roles": ["access", "staff", "superuser"],
}
EOF
}

resource "authentik_provider_oauth2" "public-test-client-oidc" {
  name               = "public-test-client-oidc"
  authorization_flow = data.authentik_flow.default-provider-authorization-explicit-consent.id
  invalidation_flow = data.authentik_flow.default-provider-invalidation-flow.id

  client_id          = random_string.public-test-client-oidc[0].result
  client_secret      = random_string.public-test-client-oidc[1].result

  access_code_validity  = "hours=1"
  access_token_validity = "days=30"

  sub_mode = "user_email"

  allowed_redirect_uris = flatten([
    for protocol in ["http", "https"] : [
      for host in ["localhost", "127.0.0.1", "[::1]"] : [
        for port in [8000, 8080] : [
          for path in ["complete/prologin", "complete/oidc", "accounts/complete/prologin", "api/accounts/complete/prologin", "api/auth/callback/prologin", "rest/auth/oidc/callback", "callback"] : [
            for trailing_slash in ["", "/"] :
              {
                matching_mode = "strict",
                url           = "${protocol}://${host}:${port}/${path}${trailing_slash}",
              }
          ]
        ]
      ]
    ]
  ])

  property_mappings = [
    data.authentik_property_mapping_provider_scope.scope-email.id,
    data.authentik_property_mapping_provider_scope.scope-openid.id,
    data.authentik_property_mapping_provider_scope.scope-profile.id,
    authentik_property_mapping_provider_scope.public-test-client_roles.id,
  ]

  signing_key = data.authentik_certificate_key_pair.default.id
}

resource "authentik_application" "public-test-client-oidc" {
  name               = "Prologin public test client"
  slug               = "prologin-public-test-client"
  group              = "Staff"
  protocol_provider  = authentik_provider_oauth2.public-test-client-oidc.id
  policy_engine_mode = "any"
  meta_launch_url    = "blank://blank"
}

resource "authentik_policy_binding" "public-test-client_group-filtering" {
  for_each = { for idx, value in local.public_test_client_allowed_groups : idx => value }

  target = authentik_application.public-test-client-oidc.uuid
  group  = data.authentik_group.groups[each.value].id
  order  = each.key
}
