# This file is maintained automatically by "tofu init".
# Manual edits may be lost in future updates.

provider "registry.opentofu.org/goauthentik/authentik" {
  version     = "2024.10.2"
  constraints = "2024.10.2"
  hashes = [
    "h1:qjDOLb8+12kZHSM3VsItQCsZYJhDMD4bNKSZi15HQ28=",
    "zh:06c6c9bb2716052fefc1013ed1a77a12159d5625fe43857700c282e80e2fbba1",
    "zh:121e45b3d3675df24e2c1bb107e2ed15fc9f1ec8b602b9bdaebec71481addf0c",
    "zh:2aec74c8df3e3eb56fb09edcb1c7f43c91f932b2ef2327aa855ba0819f11169e",
    "zh:4f2bf009f43293a24cc8941d4bbab340a53f569a9331aa615a7934f500a64290",
    "zh:64b150655b47c60e6ae72a2ee754f5019b2baabd4dc292a6b2b960b3a206e218",
    "zh:78bf3fd7cbac489d23a620743e5af5b85b31fc548433cf86f0861878b68f2666",
    "zh:7ce7a02671056d476d17652d780ee2bd309ce34eb77746719b7b277ca66b7c58",
    "zh:84fdb911186918cbba86c1390ce18a4423f0d748216f2d9c8421801b34b41f16",
    "zh:95db38fb110302707cd70471f5cb2bf361ed6d5987f7b6fe5f3c5855f9dc9b64",
    "zh:9c24dbf6512637bb1d4201a901dddef0210b440ad8b02717ca1167b75afa6882",
    "zh:a83bc8bfe87e44c788c3c974e764c7bfb1c5fb982f427a5b928c50e55b48dea6",
    "zh:b5a4d5d1f2f0e8d65ad29a23bfd72d0d4e3e06e9bacea9463a10e67137833409",
    "zh:d1e08a662ab7c80373bc13446c9b316a671fcddec6aeffef7ab3649d1bbfb76b",
    "zh:e1c50a791f2d53f7b464ab122f92062547d5a4ad71297f5e7f0375453cd2034f",
  ]
}

provider "registry.opentofu.org/hashicorp/random" {
  version     = "3.4.3"
  constraints = "3.4.3"
  hashes = [
    "h1:57xIMCTAE78wv9naPFb3atFFEFn3rW1hOFYTrXMk/C0=",
    "zh:40f2ab718f177b0f8ec29da906104583047531de32cd7dc7f005a606a099d474",
    "zh:6a6684084bd1624b93a262663c5849f9efb597c99d6b03eeb6dbd685760561fb",
    "zh:8dc1973537166b468c08526ef38fa353f389df4ae9639cf8591dbc6e6048336b",
    "zh:aa260ea7793988e7f45ea3916ed6e177f8827e9dd3959fe799cbeda2329e7d23",
    "zh:b59bbf92b7be796c1921acf22b40fbb5e699bd3e9bdc06fa27a7e273509e1028",
    "zh:c4603614ca8c73f3c9b00cb4e89d3b859a62864cf09faba2ae376689a354f326",
    "zh:d82ba8432161763525dc7e8f32ac2377ea444829f805511cb90369b147c62b0c",
    "zh:ee058d8839b35e1dbcfea224652e6e921e015d3454e0c06e8afce3516bb50910",
    "zh:efffc2adfbfdbfde4f7fc9f423338c5969054de064b7afcd391fa2c419da2bc2",
    "zh:f0530bdae9985a3be630679d6c29396721616148a08594bb0a55551a69c53c13",
  ]
}

provider "registry.opentofu.org/hashicorp/vault" {
  version     = "3.12.0"
  constraints = "3.12.0"
  hashes = [
    "h1:3eN2Lod82+phqq3+Lw7X5QLU2yruHjT9joTrD9cB6g4=",
    "zh:07c6dcfd93ace5f357aaeb02b53a6838e4471409c712bfeff5f747e7ec896445",
    "zh:16cb2adee826377f9826af6b721dd93e9209e1128b20ea0461b2b658f6b24dfa",
    "zh:2009ee2bcfab634d1905b9fe304c2014bf4c5003d6723700ddbc808b6b5a37d0",
    "zh:3181c9f6939d0f50d6f334420795b2d7b4b40f4c7d5d80d657f1c1aaddbf5be4",
    "zh:4516a40d49868e6ddf83b551342c6f288ffa60199bfb72f149dfb8e000004777",
    "zh:8b1ef18b46895e3f234fee851b151bc995e0723d2c6cfed55e1febc659f5390f",
    "zh:9c4d466c85b0a7d7f7479a5f8f0d2fef81b78c9c1e4f405d49dfa1e5d90515bb",
    "zh:a0d7e6cff652bf1143db8469d74ee37fab8171a74111697ab07587fd5477d640",
    "zh:e175d3c1847ae1a4b572bbda322e7ac9c6957f67bb744976b385e5eda7734d6a",
    "zh:fbf59ec5d3dbbd5d0484971e4f0d6a39a5db176553ec7fcb700f442775c5e698",
  ]
}
