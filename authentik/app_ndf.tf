locals {
  ndf_allowed_groups = [
    "staff",
  ]
  ndf_superuser_groups = [
    "presidence",
    "tresorerie",
  ]
  ndf_staff_groups = [
    "bureau",
  ]
}


resource "random_string" "ndf-oidc" {
  count   = 2
  length  = 64
  special = false
}

resource "authentik_property_mapping_provider_scope" "ndf_roles" {
  name       = "NDF Roles"
  scope_name = "roles"
  expression = <<EOF
roles = []

%{for group in local.ndf_superuser_groups}
if ak_is_group_member(request.user, name="${group}"):
    roles.append("superuser")
%{endfor}

%{for group in local.ndf_staff_groups}
if ak_is_group_member(request.user, name="${group}"):
    roles.append("staff")
%{endfor}

return {
  "roles": list(set(roles)),
}
EOF
}

resource "authentik_provider_oauth2" "ndf-oidc" {
  name               = "ndf-oidc"
  authorization_flow = data.authentik_flow.default-provider-authorization-implicit-consent.id
  invalidation_flow = data.authentik_flow.default-provider-invalidation-flow.id
  client_id          = random_string.ndf-oidc[0].result
  client_secret      = random_string.ndf-oidc[1].result

  access_code_validity  = "hours=1"
  access_token_validity = "days=1"

  sub_mode = "user_email"

  allowed_redirect_uris = [
    {
      matching_mode = "strict",
      url           = "https://ndf.prologin.org/rest/auth/oidc/callback/",
    }
  ]

  property_mappings = [
    data.authentik_property_mapping_provider_scope.scope-email.id,
    data.authentik_property_mapping_provider_scope.scope-openid.id,
    data.authentik_property_mapping_provider_scope.scope-profile.id,
    authentik_property_mapping_provider_scope.ndf_roles.id,
  ]

  signing_key = data.authentik_certificate_key_pair.default.id
}

resource "authentik_application" "ndf-oidc" {
  name               = "NDF"
  slug               = "ndf"
  group              = "Staff"
  protocol_provider  = authentik_provider_oauth2.ndf-oidc.id
  policy_engine_mode = "any"

  meta_launch_url  = "https://ndf.prologin.org"
  meta_icon        = "https://ndf.s3.prologin.org/static/favicon.png"
  meta_description = "Gestion des notes de frais"
}

resource "authentik_policy_binding" "ndf_group-filtering" {
  for_each = { for idx, value in local.ndf_allowed_groups : idx => value }

  target = authentik_application.ndf-oidc.uuid
  group  = data.authentik_group.groups[each.value].id
  order  = each.key
}

resource "vault_generic_secret" "authentik_apps_ndf_oidc" {
  path = "authentik/apps/ndf/oidc"
  data_json = jsonencode({
    client_id = authentik_provider_oauth2.ndf-oidc.client_id
    secret_id = authentik_provider_oauth2.ndf-oidc.client_secret
  })
}


resource "authentik_user" "ndf" {
  username = "ndf"
  email    = "ndf@prologin.org"
  name     = "NDF"
  path     = "users/prologin/service_accounts"
}

resource "authentik_token" "ndf" {
  identifier = "ndf"
  user       = authentik_user.ndf.id
  intent     = "app_password"
  expiring   = false

  retrieve_key = true
}

resource "vault_generic_secret" "authentik_apps_ndf_user" {
  path = "authentik/apps/ndf/user"
  data_json = jsonencode({
    username = authentik_user.ndf.username
    password = authentik_token.ndf.key
  })
}
