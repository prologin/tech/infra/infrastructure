

resource "authentik_provider_proxy" "legacy_gcc_site" {
  name               = "GCC! Site (Old)"
  authorization_flow = data.authentik_flow.default-provider-authorization-implicit-consent.id
  invalidation_flow = data.authentik_flow.default-provider-invalidation-flow.id

  mode               = "forward_single"
  external_host      = "https://gcc-legacy.prologin.dev"
}

resource "authentik_application" "legacy_gcc_site" {
  name              = "GCC! Site (Old)"
  slug              = "legacy-gcc-site"
  group             = "Staff"
  protocol_provider = authentik_provider_proxy.legacy_gcc_site.id

  meta_launch_url  = "https://gcc-legacy.prologin.dev"
  meta_description = "GCC! Site (Old)"
}

resource "authentik_policy_binding" "legacy_gcc_site-group-filtering" {
  for_each = { for idx, value in ["roots", "bureau", "respo-regionaux"] : idx => value }
  target   = authentik_application.legacy_gcc_site.uuid
  group    = data.authentik_group.groups[each.value].id
  order    = each.key
}
