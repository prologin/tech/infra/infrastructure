resource "authentik_brand" "prologin_org" {
  domain  = "prologin.org"
  default = false

  branding_title   = "Prologin"
  branding_logo    = "https://prologin.org/static/img/logo_cube.png"
  branding_favicon = "https://prologin.org/static/img/favicon-194.png"
}

resource "authentik_brand" "girlscancode_fr" {
  domain  = "girlscancode.fr"
  default = false

  branding_title   = "Girls Can Code!"
  branding_logo    = "https://girlscancode.fr/static/img/logo_gcc_blacktext.png"
  branding_favicon = "https://girlscancode.fr/static/favicon.svg"
}
