resource "authentik_application" "ateliers-contenus" {
  name  = "Ateliers Prologin"
  slug  = "ateliers-contenus"
  group = "Prologin"

  meta_launch_url  = "https://ateliers.prologin.org"
  meta_icon        = "https://prologin.org/static/img/favicon-194.png"
  meta_description = "Contenus des ateliers Prologin"
}
