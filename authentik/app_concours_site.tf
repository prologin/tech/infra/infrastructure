resource "authentik_application" "concours-site" {
  name  = "Prologin"
  slug  = "concours-site"
  group = "Prologin"

  meta_launch_url  = "https://prologin.org"
  meta_icon        = "https://prologin.org/static/img/logo_cube.png"
  meta_description = "Concours Prologin"
}
