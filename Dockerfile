FROM python:3.12-slim as base

ENV DEBIAN_FRONTEND noninteractive
ENV LC_ALL C.UTF-8
ENV LANG C.UTF-8
ENV PYTHONDONTWRITEBYTECODE=1 \
    PYTHONFAULTHANDLER=1 \
    PYTHONUNBUFFERED=1 \
    PYTHONHASHSEED=random \
    PIP_NO_CACHE_DIR=off \
    PIP_DEFAULT_TIMEOUT=100 \
    POETRY_VIRTUALENVS_CREATE=false \
    PATH="/opt/venv/bin:/root/.cargo/bin:$PATH"

RUN apt-get update && \
    apt-get install -y --no-install-recommends \
            curl ca-certificates gnupg apt-transport-https git software-properties-common wget

# Golang
RUN apt-get install -y --no-install-recommends golang-go

# mdBook
RUN apt-get install -y --no-install-recommends gcc-multilib libssl-dev pkg-config perl make && \
    curl -1sLf https://sh.rustup.rs | sh -s -- --profile minimal --default-toolchain stable -y
RUN cargo install mdbook mdbook-linkcheck mdbook-mermaid

# Shellcheck
RUN apt-get install -y --no-install-recommends shellcheck

# OpenTofu
RUN curl -Lo /tmp/tofu.tar.gz --fail https://github.com/opentofu/opentofu/releases/download/v1.8.2/tofu_1.8.2_linux_amd64.tar.gz && \
    [ "$(sha256sum /tmp/tofu.tar.gz)" = "9f43888571cbe9cdfe03e81c6cbb93b4aacc014db19373a4e7608aaa7df111d1  /tmp/tofu.tar.gz" ] && \
    tar -C /usr/local/bin -xvf /tmp/tofu.tar.gz tofu && \
    rm /tmp/tofu.tar.gz

FROM base as builder

RUN apt-get update && \
    apt-get install -y --no-install-recommends gcc python-dev-is-python3 git

RUN --mount=type=bind,target=./pyproject.toml,src=./pyproject.toml \
    --mount=type=bind,target=./poetry.lock,src=./poetry.lock \
    --mount=type=cache,target=/root/.cache/pip \
    python -m venv /opt/venv && \
    pip3 install --upgrade pip && \
    pip3 install --upgrade poetry && \
    . /opt/venv/bin/activate && \
    poetry install

FROM base

COPY --from=builder /opt/venv/ /opt/venv/
