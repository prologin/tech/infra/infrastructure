# Infrastructure

Welcome to Prologin infrastructure repository. You can find more information
about the infrastructure and this repository in [our
documentation](https://prologin.gitlab.io/tech/infra/infrastructure/).

## License

This repository is licensed under the [AGPL-3.0-or-later](./LICENSE) license.

## Acknowledgements

Some of the code in this repository is taken from elsewhere. Have a look in the
[acknowledgements](./ACKNOWLEDGEMENTS.md) file for an exhaustive list.
