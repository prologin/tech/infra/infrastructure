# This file is maintained automatically by "tofu init".
# Manual edits may be lost in future updates.

provider "registry.opentofu.org/grafana/grafana" {
  version     = "1.34.0"
  constraints = "1.34.0"
  hashes = [
    "h1:qDdYL7jTULsQTKB/Ucn6VSczJ9QdFgHpio83mOUPhTE=",
    "zh:28526e56e43196ecff25a3cba219280fdb4983554217eb138e0cef623ee6f24a",
    "zh:451bc35d6586b7f4177546e3a77dd3bc76f1e2c2547152fa78ade7875e2052a8",
    "zh:6e84d283ed9a9892f93cabff22637cf9e35e143b1cf0410be304cd15b7af663f",
    "zh:7fa8e0c65c5f728192f6d3848921d08b78b34b91eca75ea9bc8e99471184738d",
    "zh:845dd145f5b1de88a13d2306616344a28920d6b3729a373a36a5efcc504dce70",
    "zh:90c069a658da6985da3039b6a137ad0049e667a98988c298c2bc9613ee5b9a89",
    "zh:b333662417527c8ef3e2009523dcb1e3b9dd4ca20f38a0ea0c505928c9d6530c",
    "zh:b4e2c8b74c806c410540090aaffe0346b3c16722b9abef6d5d7b4d3dba01433d",
    "zh:c1ca0ddbf01100c27e711e96de1f291d19a848a9f6864ba828a8a12483b5f813",
    "zh:cc781151cf1752e250a838a0a8db88a87e03c41e5dd989a9a851dd9269e663e3",
    "zh:def0c627d58b3ae8c023657f87402691278c0d5bd151d7fdbd98c5acd84415e1",
    "zh:e4b5fc135460ef560496fe296c2eb53bb69c30e2eab24d3f161b871014bec764",
    "zh:e8415fde2f4dac220453040e435564defdb595721b1464d84f16c5e398431d9a",
    "zh:f4e5ba3e75277612ee06426ed6af3bd180d4b9f19e38623cd244d9d85b3345c4",
  ]
}

provider "registry.opentofu.org/hashicorp/vault" {
  version     = "3.12.0"
  constraints = "3.12.0"
  hashes = [
    "h1:3eN2Lod82+phqq3+Lw7X5QLU2yruHjT9joTrD9cB6g4=",
    "zh:07c6dcfd93ace5f357aaeb02b53a6838e4471409c712bfeff5f747e7ec896445",
    "zh:16cb2adee826377f9826af6b721dd93e9209e1128b20ea0461b2b658f6b24dfa",
    "zh:2009ee2bcfab634d1905b9fe304c2014bf4c5003d6723700ddbc808b6b5a37d0",
    "zh:3181c9f6939d0f50d6f334420795b2d7b4b40f4c7d5d80d657f1c1aaddbf5be4",
    "zh:4516a40d49868e6ddf83b551342c6f288ffa60199bfb72f149dfb8e000004777",
    "zh:8b1ef18b46895e3f234fee851b151bc995e0723d2c6cfed55e1febc659f5390f",
    "zh:9c4d466c85b0a7d7f7479a5f8f0d2fef81b78c9c1e4f405d49dfa1e5d90515bb",
    "zh:a0d7e6cff652bf1143db8469d74ee37fab8171a74111697ab07587fd5477d640",
    "zh:e175d3c1847ae1a4b572bbda322e7ac9c6957f67bb744976b385e5eda7734d6a",
    "zh:fbf59ec5d3dbbd5d0484971e4f0d6a39a5db176553ec7fcb700f442775c5e698",
  ]
}
