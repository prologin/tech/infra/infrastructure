locals {
  gcc_senders = toset([
    "gccsite_prod"
  ])
}

resource "aws_iam_user" "gcc_smtp_users" {
  for_each = local.gcc_senders
  name     = "ses-girlscancode-${each.key}"
}

resource "aws_iam_access_key" "gcc_smtp_users" {
  for_each = aws_iam_user.gcc_smtp_users
  user     = each.value.name
}

data "aws_ses_email_identity" "info_gcc_smtp_email_identity" {
  email = "info@girlscancode.fr"
}

data "aws_iam_policy_document" "gcc_ses_sender" {
  statement {
    actions   = ["ses:SendRawEmail"]
    resources = ["${data.aws_ses_email_identity.info_gcc_smtp_email_identity.arn}"]
  }
}

resource "aws_iam_policy" "gcc_ses_sender" {
  name   = "info-girlscancode-ses-sender"
  policy = data.aws_iam_policy_document.gcc_ses_sender.json
}

resource "aws_iam_user_policy_attachment" "gcc_ses_sender" {
  for_each   = aws_iam_user.gcc_smtp_users
  user       = each.value.name
  policy_arn = aws_iam_policy.gcc_ses_sender.arn
}

resource "vault_generic_secret" "infra_aws_gcc_ses_senders" {
  for_each = local.gcc_senders

  path = "infra/aws/ses/gcc-senders/${each.key}"
  data_json = jsonencode({
    username = aws_iam_access_key.gcc_smtp_users[each.key].id
    password = aws_iam_access_key.gcc_smtp_users[each.key].ses_smtp_password_v4
  })
}
