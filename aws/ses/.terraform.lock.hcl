# This file is maintained automatically by "tofu init".
# Manual edits may be lost in future updates.

provider "registry.opentofu.org/cloudflare/cloudflare" {
  version     = "3.32.0"
  constraints = "3.32.0"
  hashes = [
    "h1:m+MuihUEa0RARMGxpGKAOeCq99d94njRXJjKCAc6Xtk=",
    "zh:0be6ee63a380c7cf8b0666dd296ab5cdb9ec0a18ae99cd11d732783debd783f4",
    "zh:0dca442861a263aaadf5c95ce962b979b8380c9c6e472018cba345aa9b6484ef",
    "zh:549b44da944698d07d58d678f528e14d81c76d8e16d0dcab3d47a2956b20c2dd",
    "zh:604206dca9896baec3759c34d83477535eaba9c40843d299bf5dd302830883fd",
    "zh:6bff7b21254f218eba7da0227694abe33de7750a59d8d54dd04c814a0b5fe3dc",
    "zh:7364c2bbae08208384831ccad983963c9746a83ac02e8061b6cc78407b202605",
    "zh:7fba3591440ef6485eac5ab5794f7f43b4e0195365b5451bac29bd2dbccdbe14",
    "zh:844a6ede2b60df8507865b0b2c137c76412ec55e8601ca132c113bc5d4d5f594",
    "zh:90947dd9bfe6a5ab0b77c6c36bbbf07d67c94d6d22cc4fbe3c7572accda7f9b4",
    "zh:987fd764c9f2595eba98774fa07bb669ae97546e06289b10a5536f1c1c2cb618",
    "zh:993c8b9e7ab31ac39cd586a07578113341bb5870bc2348875a4ad4f2234efe0e",
    "zh:be77e1575e93485e8a507e995e5f6cefc9f14681dc26396813cbf079fda87c20",
    "zh:c300598e693c177f8a6dd3ff42e9f95cbaf7789d77124ad48899b9f4f8400ec0",
    "zh:f589e8754cb4dc6baf43c0f08004073bb2e8a703aa71680f9ac2efd49fdc7bdf",
  ]
}

provider "registry.opentofu.org/hashicorp/aws" {
  version     = "4.52.0"
  constraints = "4.52.0"
  hashes = [
    "h1:/nnnCW3k4Z84TgdnkFwMLlWXqY3k3Ac7KdmnS2V5jbU=",
    "zh:438d3f671be41fc1e724b9bd295f247c704e2a1a01c93b2ded896911c84b2401",
    "zh:4cb8cfabc720c54b8c5c97dcc79fd87399a859f51a860cf5673d62e3476a0b54",
    "zh:647ee988a871d099b6ea51a46c20eb8a653d7ee4c42167c2e101a8442ded64c3",
    "zh:8661d5bfd64e76892a8a1a408c02cce8b07483163dbcf40d5b1c16a179f6337e",
    "zh:a6a1db583f1ad63dafa4908c619d2bc08528be7ada3ce491656c4c62b87649cc",
    "zh:aa7ca15c822e066b9ee635a4e97636fd739d44a42b050c181671d0378b863ede",
    "zh:b0551b846244202a87b121d7d83e13a307a5056b1497881b79bc5d4e13f29e23",
    "zh:b5646989d19ae91976a27d7e812d969298e4e370bc1eb8f6348c9fd96c384e62",
    "zh:f39cb220006816e61eae548fcfeb8da8a65a0a9f8a119dd6b6bb57b696a566b8",
    "zh:fb5093246c4f200dc485999cc9ebad69727fe56cbcb9793391e84cca003c506b",
  ]
}

provider "registry.opentofu.org/hashicorp/vault" {
  version     = "3.12.0"
  constraints = "3.12.0"
  hashes = [
    "h1:3eN2Lod82+phqq3+Lw7X5QLU2yruHjT9joTrD9cB6g4=",
    "zh:07c6dcfd93ace5f357aaeb02b53a6838e4471409c712bfeff5f747e7ec896445",
    "zh:16cb2adee826377f9826af6b721dd93e9209e1128b20ea0461b2b658f6b24dfa",
    "zh:2009ee2bcfab634d1905b9fe304c2014bf4c5003d6723700ddbc808b6b5a37d0",
    "zh:3181c9f6939d0f50d6f334420795b2d7b4b40f4c7d5d80d657f1c1aaddbf5be4",
    "zh:4516a40d49868e6ddf83b551342c6f288ffa60199bfb72f149dfb8e000004777",
    "zh:8b1ef18b46895e3f234fee851b151bc995e0723d2c6cfed55e1febc659f5390f",
    "zh:9c4d466c85b0a7d7f7479a5f8f0d2fef81b78c9c1e4f405d49dfa1e5d90515bb",
    "zh:a0d7e6cff652bf1143db8469d74ee37fab8171a74111697ab07587fd5477d640",
    "zh:e175d3c1847ae1a4b572bbda322e7ac9c6957f67bb744976b385e5eda7734d6a",
    "zh:fbf59ec5d3dbbd5d0484971e4f0d6a39a5db176553ec7fcb700f442775c5e698",
  ]
}
