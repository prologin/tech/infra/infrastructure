data "cloudflare_zone" "prologin_org" {
  name = "prologin.org"
}

resource "aws_ses_domain_identity" "prologin_org" {
  domain = "prologin.org"
}

resource "aws_ses_domain_dkim" "prologin_org" {
  domain = aws_ses_domain_identity.prologin_org.domain
}

resource "cloudflare_record" "prologin_org_dkim" {
  for_each = toset(aws_ses_domain_dkim.prologin_org.dkim_tokens)

  zone_id = data.cloudflare_zone.prologin_org.id
  name    = "${each.value}._domainkey"
  type    = "CNAME"
  value   = "${each.value}.dkim.amazonses.com"
}

resource "aws_ses_domain_identity_verification" "prologin_org" {
  domain = aws_ses_domain_identity.prologin_org.id

  depends_on = [
    cloudflare_record.prologin_org_dkim,
  ]
}
