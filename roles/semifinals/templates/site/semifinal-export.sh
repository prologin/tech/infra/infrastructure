#! /bin/sh

[ "$#" -ne "1" ] && echo 1>&2 "usage: $0 event_id" && exit 1

cd "/home/prologin/site/prologin"
export SEMIFINALS_INSTANCE_ID="$1"
export DJANGO_SETTINGS_MODULE="prologin.settings.prod"
shift 1
export="/srv/prologin/instances/$SEMIFINALS_INSTANCE_ID/data/export.json"
exec /home/prologin/site/venv/bin/python ./manage.py semifinal_export "$export"
