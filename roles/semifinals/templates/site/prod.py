from .semifinal_common import *
import os
import json
from pathlib import Path

INSTALLED_APPS += ("djmail",)

SEMIFINALS_INSTANCE_ID = os.environ['SEMIFINALS_INSTANCE_ID']

SEMIFINALS_INSTANCE_SETTINGS_PATH = "/srv/prologin/instances/%(id)s/site.json"

with open("/srv/prologin/global/site.json") as f:
    SEMIFINALS_GLOBAL_SETTINGS = json.load(f)

with open(SEMIFINALS_INSTANCE_SETTINGS_PATH % {'id': SEMIFINALS_INSTANCE_ID}) as f:
    SEMIFINALS_INSTANCE_SETTINGS = json.load(f)

# You can use $ pwgen -y 64
SECRET_KEY = SEMIFINALS_INSTANCE_SETTINGS['django_secret_key']

# SECURITY/PERFORMANCE WARNING: don't run with DEBUG turned on in production!
DEBUG = SEMIFINALS_INSTANCE_SETTINGS['debug']

ALLOWED_HOSTS = SEMIFINALS_INSTANCE_SETTINGS['allowed_hosts']
INTERNAL_IPS = ALLOWED_HOSTS

# Set the right hostname, as seen by contestant's machines
SITE_HOST = SEMIFINALS_INSTANCE_SETTINGS['site_host']

# Current edition
PROLOGIN_EDITION = 2025

# Repository paths
PROBLEMS_REPOSITORY_PATH = os.path.join(PROJECT_ROOT_DIR, '..', 'problems')

# Camisole url
PROBLEMS_CORRECTORS = ('http://192.168.122.41:42920/run',)

# Statics
STATIC_ROOT = "/home/prologin/public/static"


# These should be OK (assuming no TLS)
PROBLEMS_CHALLENGE_WHITELIST = ('demi{}'.format(PROLOGIN_EDITION),)
SITE_BASE_URL = 'https://{}'.format(SITE_HOST)

# Uncomment and modify if needed:

# Time before a new problem is automatically unlocked to help pass the
# current level
# PROBLEMS_DEFAULT_AUTO_UNLOCK_DELAY = 15 * 60

# How much time spent on a problem becomes concerning
# Format: a tuple of (warning amount, danger amount), amounts in seconds
# SEMIFINAL_CONCERNING_TIME_SPENT = (30 * 60, 45 * 60)

# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': f'semifinals_{SEMIFINALS_INSTANCE_ID}',
        'USER': SEMIFINALS_GLOBAL_SETTINGS['db_user'],
        'PASSWORD': SEMIFINALS_GLOBAL_SETTINGS['db_password'],
        'HOST': 'postgresql.th2.hxg.prologin.dev',
        'PORT': 5432,
    }
}

# Redis Broker (Celery)
BROKER_URL = f'redis://redis.th2.hxg.prologin.dev:6390/{SEMIFINALS_INSTANCE_ID}'
CELERY_RESULT_BACKEND = f'redis://redis.th2.hxg.prologin.dev:6390/{SEMIFINALS_INSTANCE_ID}'
PROLOGIN_UTILITY_REDIS_STORE = dict(
    host='redis.th2.hxg.prologin.dev',
    port=6390,
    db=int(SEMIFINALS_INSTANCE_ID),
    socket_connect_timeout=3,
    socket_timeout=4
)
