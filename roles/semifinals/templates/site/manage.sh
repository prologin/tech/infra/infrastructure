#! /bin/sh

[ "$#" -lt "1" ] && echo 1>&2 "usage: $0 event_id [args ...]" && exit 1

cd "/home/prologin/site/prologin"
export SEMIFINALS_INSTANCE_ID="$1"
export DJANGO_SETTINGS_MODULE="prologin.settings.prod"
shift 1
exec /home/prologin/site/venv/bin/python ./manage.py "$@"
