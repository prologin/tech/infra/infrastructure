#! /bin/sh

set -e

export SEMIFINALS_INSTANCE_DIR="/srv/prologin/instances/$SEMIFINALS_INSTANCE_ID"

/home/prologin/site/venv/bin/python ./manage.py migrate --noinput
/home/prologin/site/venv/bin/python ./manage.py collectstatic --noinput
/home/prologin/site/venv/bin/python ./manage.py semifinal_bootstrap "$SEMIFINALS_INSTANCE_DIR/data/import.json" || true
