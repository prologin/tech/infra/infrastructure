user www-data;
worker_processes auto;
pid /run/nginx.pid;
error_log /var/log/nginx/error.log;
include /etc/nginx/modules-enabled/*.conf;

events {
    worker_connections 768;
}

http {

    ##
    # Basic Settings
    ##

    sendfile on;
    tcp_nopush on;
    types_hash_max_size 2048;
    server_tokens off;

    include /etc/nginx/mime.types;
    default_type application/octet-stream;

    ##
    # SSL Settings
    ##

    ssl_protocols TLSv1.3;
    ssl_prefer_server_ciphers on;

    access_log /var/log/nginx/access.log;
    gzip on;

    ##
    # Virtual Host Configs
    ##

    include /etc/nginx/conf.d/*.conf;

    server {
        listen 80 default_server;
        server_name _;
        return 302 https://$host$request_uri;
    }

    server {
        listen {{ semifinals_public_ip }}:443 ssl proxy_protocol;
        server_name _ default_server;
        ssl_certificate /etc/ssl/private/semifinals-internet.cert.pem;
        ssl_certificate_key /etc/ssl/private/semifinals-internet.key.pem;

        set_real_ip_from 192.168.1.254;
        real_ip_header proxy_protocol;

        default_type text/plain;
        return 404 "Nothing to see";
    }

    server {
        listen {{ semifinals_extranet_ip }}:443 ssl;
        server_name _ default_server;
        ssl_certificate /etc/ssl/private/semifinals-extranet.cert.pem;
        ssl_certificate_key /etc/ssl/private/semifinals-extranet.key.pem;

        default_type text/plain;
        return 404 "Nothing to see";
    }

    server {
        listen {{ semifinals_public_ip }}:443 ssl proxy_protocol;
        server_name monip.demi-finale.prologin.org;
        ssl_certificate /etc/ssl/private/semifinals-internet.cert.pem;
        ssl_certificate_key /etc/ssl/private/semifinals-internet.key.pem;

        set_real_ip_from 192.168.1.254;
        real_ip_header proxy_protocol;

        default_type text/plain;
        return 404 $remote_addr;
    }

    server {
        listen {{ semifinals_extranet_ip }}:443 ssl;
        listen {{ semifinals_extranet_ip }}:7443 ssl proxy_protocol;
        server_name monip.demi-finale.extranet.prologin.org;
        ssl_certificate /etc/ssl/private/semifinals-extranet.cert.pem;
        ssl_certificate_key /etc/ssl/private/semifinals-extranet.key.pem;

        set_real_ip_from 100.80.91.0/24;
        real_ip_header proxy_protocol;

        default_type text/plain;
        return 200 $remote_addr;
    }

    server {
        listen {{ semifinals_public_ip }}:443 ssl proxy_protocol;
        server_name docs.demi-finale.prologin.org;
        ssl_certificate /etc/ssl/private/semifinals-internet.cert.pem;
        ssl_certificate_key /etc/ssl/private/semifinals-internet.key.pem;

        set_real_ip_from 192.168.1.254;
        real_ip_header proxy_protocol;

        location / {
            include proxy_params;
            proxy_pass http://100.80.1.20:9292;
        }
    }

    server {
        listen {{ semifinals_extranet_ip }}:443 ssl;
        listen {{ semifinals_extranet_ip }}:7443 ssl proxy_protocol;
        server_name docs.demi-finale.extranet.prologin.org;
        ssl_certificate /etc/ssl/private/semifinals-extranet.cert.pem;
        ssl_certificate_key /etc/ssl/private/semifinals-extranet.key.pem;

        set_real_ip_from 100.80.91.0/24;
        real_ip_header proxy_protocol;

        location / {
            include proxy_params;
            proxy_pass http://100.80.1.20:9292;
        }
    }

    server {
        listen {{ semifinals_extranet_ip }}:443 ssl;
        allow 100.80.91.0/24;
        deny all;
        server_name warp.demi-finale.extranet.prologin.org;
        ssl_certificate /etc/ssl/private/semifinals-extranet.cert.pem;
        ssl_certificate_key /etc/ssl/private/semifinals-extranet.key.pem;

        {% for site in semifinals_instances -%}
        location = /{{ site.id }} {
            return 302 https://{{ site.slug }}.demi-finale.extranet.prologin.org;
        }
        {% endfor -%}
    }

    {% for instance in semifinals_instances %}
    server {
        listen {{ semifinals_public_ip }}:443 ssl proxy_protocol;
        server_name {{ instance.slug }}.demi-finale.prologin.org;
        ssl_certificate /etc/ssl/private/semifinals-internet.cert.pem;
        ssl_certificate_key /etc/ssl/private/semifinals-internet.key.pem;

        set_real_ip_from 192.168.1.254;
        real_ip_header proxy_protocol;

        access_log /var/log/nginx/{{ instance.slug }}.demi-finale.prologin.org.access.log;
        error_log /var/log/nginx/{{ instance.slug }}.demi-finale.prologin.org.error.log;

        root /home/prologin/public;

        satisfy any;
        {% for net in instance.allowed_networks -%}
        allow {{ net }};
        {% endfor -%}
        deny all;
        auth_basic "PROTECTED";
        auth_basic_user_file /srv/prologin/instances/{{ instance.id }}/basic_auth;

        error_page 502 =503 /503.html;

        location /metrics {
            return 403;
        }

        location /media/ {
            access_log off;
            add_header Cache-Control "public";
            expires 7d;
        }

        location /static/ {
            access_log off;
            add_header Cache-Control "public";
            expires 7d;
        }

        location / {
            try_files $uri @django;
        }

        location @django {
            include proxy_params;
            proxy_pass http://unix:/run/prologin.{{ instance.id }}/django.sock;
        }
    }

    server {
        listen {{ semifinals_extranet_ip }}:443 ssl;
        listen {{ semifinals_extranet_ip }}:7443 ssl proxy_protocol;

        set_real_ip_from 100.80.91.0/24;
        real_ip_header proxy_protocol;

        server_name {{ instance.slug }}.demi-finale.extranet.prologin.org;
{% if instance.id == 358 %}
        server_name 129.104.254.15;
{% endif %}
        ssl_certificate /etc/ssl/private/semifinals-extranet.cert.pem;
        ssl_certificate_key /etc/ssl/private/semifinals-extranet.key.pem;

        access_log /var/log/nginx/{{ instance.slug }}.demi-finale.extranet.prologin.org.access.log;
        error_log /var/log/nginx/{{ instance.slug }}.demi-finale.extranet.prologin.org.error.log;

        root /home/prologin/public;

        satisfy any;
        allow 100.80.91.0/24;
        {% for net in instance.allowed_networks -%}
        allow {{ net }};
        {% endfor -%}
        deny all;
        auth_basic "PROTECTED";
        auth_basic_user_file /srv/prologin/instances/{{ instance.id }}/basic_auth;

        error_page 502 =503 /503.html;

        location /metrics {
            return 403;
        }

        location /media/ {
            access_log off;
            add_header Cache-Control "public";
            expires 7d;
        }

        location /static/ {
            access_log off;
            add_header Cache-Control "public";
            expires 7d;
        }

        location / {
            try_files $uri @django;
        }

        location @django {
            include proxy_params;
            proxy_pass http://unix:/run/prologin.{{ instance.id }}/django.sock;
        }
    }



    {% endfor %}
}
