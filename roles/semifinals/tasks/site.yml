---

- name: create prologin user
  ansible.builtin.user:
    name: prologin
    create_home: true
    shell: /bin/bash

- name: create .ssh
  ansible.builtin.file:
    path: /home/prologin/.ssh
    mode: 0700
    owner: prologin
    group: prologin
    state: directory

- name: copy ssh deploy key
  become: true
  become_user: prologin
  ansible.builtin.copy:
    dest: "/home/prologin/.ssh/{{ item.filename }}"
    mode: 0600
    owner: prologin
    group: prologin
    content: |
      {{ lookup('community.hashi_vault.hashi_vault', 'apps/data/semifinals/ssh-key:' + item.field) }}

  with_items:
    - filename: id_ed25519
      field: private_key
    - filename: id_ed25519.pub
      field: public_key

- name: install site deps
  ansible.builtin.apt:
    pkg:
      - npm
      - nginx
      - postgresql-client
      - libpq-dev
      - imagemagick
      - python3-venv
      - python3-pip
    state: present
    update_cache: true

- name: clone the site repository
  become_user: prologin
  become: true
  ansible.builtin.git:
    repo: 'https://gitlab.com/prologin/concours/site.git'
    dest: /home/prologin/site
    update: yes
    version: semifinals-auto-setup

- name: clone satellite repositories
  become_user: prologin
  become: true
  ansible.builtin.git:
    repo: '{{ item.repo }}'
    dest: '/home/prologin/{{ item.name }}'
    update: yes
    accept_hostkey: true
  with_items:
    - name: problems
      repo: git@gitlab.com:prologin/concours/problems.git
    - name: archives
      repo: git@gitlab.com:prologin/concours/archives.git
    - name: documents
      repo: git@gitlab.com:prologin/asso/documents.git

- name: setup virtualenv
  become: true
  become_user: prologin
  ansible.builtin.pip:
    requirements: /home/prologin/site/requirements.txt
    virtualenv: /home/prologin/site/venv
    virtualenv_command: /usr/local/python-3.9.21/bin/python -m venv

- name: install extra python packages
  become: true
  become_user: prologin
  ansible.builtin.pip:
    name: "{{ item }}"
    virtualenv: /home/prologin/site/venv
    virtualenv_command: /usr/local/python-3.9.21/bin/python -m venv
  with_items:
    - gunicorn

- name: install NPM dependencies
  become: true
  become_user: prologin
  community.general.npm:
    path: /home/prologin/site/assets

- name: copy configuration file
  ansible.builtin.template:
    src: site/prod.py
    dest: /home/prologin/site/prologin/prologin/settings/prod.py
    mode: 0640
    owner: prologin
    group: prologin

- name: create public directory
  ansible.builtin.file:
    path: /home/prologin/public
    state: directory
    owner: prologin
    group: www-data
    mode: 0750

- name: install scripts
  ansible.builtin.template:
    src: "site/{{ item }}"
    dest: "/srv/prologin/{{ item }}"
    owner: root
    group: prologin
    mode: 0750
  with_items:
    - prestart.sh
    - manage.sh
    - semifinal-export.sh

- name: install systemd units
  ansible.builtin.template:
    src: "systemd/{{ item }}"
    dest: "/etc/systemd/system/{{ item }}"
    mode: 0644
    owner: root
    group: root
  with_items:
    - site@.service
    - site-celeryworker@.service
  notify: systemd daemon reload
