#!/usr/bin/env python3

import http.server
import json
from os import environ
from urllib.parse import parse_qs, urlparse

import requests

HEADSCALE_ENDPOINT = environ.get("HEADSCALE_ENDPOINT")
HEADSCALE_APIKEY = environ.get("HEADSCALE_APIKEY")

PORT = int(environ.get("LISTEN_PORT", 9000))

EXCLUDE_USERS = environ.get("EXCLUDE_USERS", "").split()


class RequestHandler(http.server.BaseHTTPRequestHandler):
    protocol_version = "HTTP/1.1"

    def do_GET(self):
        url = urlparse(self.path)
        qs = parse_qs(url.query)

        port = qs.get("port", ["9100"])[-1]

        response = requests.get(
            f"{HEADSCALE_ENDPOINT}/api/v1/machine",
            headers={
                "Authorization": f"Bearer {HEADSCALE_APIKEY}",
            },
        )
        response.raise_for_status()

        machines = response.json()["machines"]

        machines = filter(lambda m: m["user"]["name"] not in EXCLUDE_USERS, machines)

        users = {}

        for machine in machines:
            user = machine["user"]["name"]
            if user in EXCLUDE_USERS:
                continue

            if not user in users:
                users[user] = []

            users[user].extend(machine["ipAddresses"])

        response = []

        for user, ips in users.items():
            response.append(
                {
                    "targets": [f"{ip}:{port}" for ip in ips],
                    "labels": {"user": user},
                }
            )

        response = json.dumps(response)
        self.send_response(200)
        self.send_header("Content-Type", "application/json")
        self.send_header("Content-Length", str(len(response)))
        self.end_headers()
        self.wfile.write(response.encode("utf-8"))


with http.server.ThreadingHTTPServer(("", PORT), RequestHandler) as httpd:
    httpd.serve_forever()
