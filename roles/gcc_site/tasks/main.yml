---
- name: Deploy Git repositories
  ansible.builtin.include_tasks: git_repository.yml
  loop:
    - url: "{{ gcc_site_repo_documents_url }}"
      dest: "/srv/{{ gcc_site_project_name }}/volumes/documents"
      version: "{{ gcc_site_repo_documents_version }}"
  loop_control:
    loop_var: repo

- name: Deploy nginx config
  ansible.builtin.include_tasks: nginx.yml

- name: Deploy gcc_site docker-compose-django
  ansible.builtin.include_role:
    name: docker_compose
  vars:
    docker_compose_project_name: "{{ gcc_site_project_name }}"

    docker_compose_networks:
      app: {}
      frontend: {}

    docker_compose_services:
      app:
        image: "{{ gcc_site_app_image }}:{{ gcc_site_app_tag }}"
        environment: "{{ gcc_site_environment_computed|combine({'INIT_JOB': '1'}) }}"
        networks:
          - app
          - frontend
        secrets:
          - django-secret-key
          - postgres-password
          - oidc-client-id
          - oidc-client-secret
          - recaptcha-public-key
          - recaptcha-private-key
          - smtp-email-user
          - smtp-email-password
          - gcc-plaintext-password-key
        volumes:
          - media_data:/var/django_media
          - static_data:/var/django_static
          - documents:/app/documents
      celery_worker:
        image: "{{ gcc_site_app_image }}:{{ gcc_site_app_tag }}"
        networks:
          - app
        environment: "{{ gcc_site_environment_computed }}"
        command: "celery -A gccsite worker -l INFO --concurrency=4"
        secrets:
          - django-secret-key
          - postgres-password
          - oidc-client-id
          - oidc-client-secret
          - recaptcha-public-key
          - recaptcha-private-key
          - smtp-email-user
          - smtp-email-password
          - gcc-plaintext-password-key
        volumes:
          - media_data:/var/django_media
          - static_data:/var/django_static
      reverse:
        image: "{{ gcc_site_reverse_image }}:{{ gcc_site_reverse_tag }}"
        command: ["nginx", "-g", "daemon off;", "-c", "/var/prologin/nginx.conf"]
        reverse_proxy:
          enable: true
          router:
            rule: "Host(`{{ gcc_site_domain }}`)"
            middlewares: default-without-error-pages@file
          service:
            port: "80"
        networks:
          - frontend
        volumes:
          - nginx_config:/var/prologin:ro
          - media_data:/var/www/html/media
          - static_data:/var/www/html/static

    docker_compose_secrets:
      django-secret-key:
        value: "{{ gcc_site_django_secret_key }}"
        env: "DJANGO_SECRET_KEY_FILE"
      postgres-password:
        value: "{{ gcc_site_postgres_password }}"
        env: "DB_PASSWORD_FILE"
      oidc-client-id:
        value: "{{ gcc_site_oidc_client_id }}"
        env: "SOCIAL_AUTH_OIDC_KEY_FILE"
      oidc-client-secret:
        value: "{{ gcc_site_oidc_client_secret }}"
        env: "SOCIAL_AUTH_OIDC_SECRET_FILE"
      recaptcha-public-key:
        value: "{{ gcc_site_recaptcha_public_key }}"
        env: "RECAPTCHA_PUBLIC_KEY_FILE"
      recaptcha-private-key:
        value: "{{ gcc_site_recaptcha_private_key }}"
        env: "RECAPTCHA_PRIVATE_KEY_FILE"
      smtp-email-user:
        value: "{{ gcc_site_email_user }}"
        env: "DJANGO_SMTP_USER_FILE"
      smtp-email-password:
        value: "{{ gcc_site_email_password }}"
        env: "DJANGO_SMTP_PASSWORD_FILE"
      gcc-plaintext-password-key:
        value: "{{ gcc_site_plaintext_password_key }}"
        env: GCC_PLAINTEXT_PASSWORD_KEY_FILE

    docker_compose_volumes_dir_owner: 1000
    docker_compose_volumes_dir_group: 101
    docker_compose_volumes_dir_mode: 0750
