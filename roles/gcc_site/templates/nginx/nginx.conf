# {{ ansible_managed }}

worker_processes 4;
error_log  /var/log/nginx/error.log debug;

events {
    worker_connections  1024;
}


http {
    include       /etc/nginx/mime.types;
    default_type  application/octet-stream;

    sendfile        on;

    keepalive_timeout  65;

    server {
        listen       *:80;
        server_name  _;

        proxy_buffer_size   128k;
        proxy_buffers   4 256k;
        proxy_busy_buffers_size   256k;
        client_max_body_size 10M;
        server_tokens off;

        root /var/www/html;

        set_real_ip_from traefik_traefik; # container address of upstream traefik

        location / {
            proxy_pass http://{{ gcc_site_project_name }}_app:8000;
            proxy_redirect off;
            proxy_set_header Host $http_host;
            proxy_set_header X-Real-IP $realip_remote_addr;
            proxy_set_header X-Forwarded-For $realip_remote_addr;
            proxy_set_header X-Forwarded-Proto https;
            proxy_set_header X-Forwarded-Host $http_host;
        }

        location /static/ {
            access_log off;
            add_header Cache-Control "public";
            expires 7d;
        }

        location /media/sponsors/ {
            access_log off;
            add_header Cache-Control "public";
            expires 7d;
        }

        location /media/ {
            internal;
        }
    }
}
