# Introduction

Welcome to the Prologin infrastructure documentation. In this book, we'll cover
everything you need to understand, and thus work on, Prologin infrastructure.

We will first cover how to setup your environment, and then we will move on to
the actual infrastructure, starting at the bottom layer, i.e. physical machines
and networks, to the top layer, i.e. applications and services running on said
machines. Although it's not strictly part of our infrastructure, we will also
go through how a number of components work for the machine rooms machines.

As such, this book is meant to be a reference for someone that does not yet
know about all the little details of (some part of) the infrastructure, and
needs to get acquainted with it to work on it.

If you are new as a Prologin root, this book will be a very interesting read as
you will discover a lot of new technologies and approaches to a number of
problems. If you are about to become root, then hopefully this book will be
your guide through your first months.

We recommend your read this book at least once front to back, without paying
much attention to the little details, but it will give you a good overview of
its contents, and it will be easier to refer to it later on.

Note that this is not supposed to be introductory material to technologies and
tools used throughout the infrastructure. However, there are times when we'll
explain required elements that might be complex to comprehend.
