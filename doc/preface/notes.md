# Notes about this book

A couple of unordered notes about conventions in this book.

The key words **must**, **must not**, **required**, **shall**, **shall not**,
**should**, **should not**, **recommended**, **not recommended**, **may**, and
**optional** in this document are to be interpreted as described in BCP 14
[RFC2119](https://datatracker.ietf.org/doc/html/rfc2119)
[RFC8174](https://datatracker.ietf.org/doc/html/rfc8174) when, and only when,
they appear in bold, as shown here. We chose to go with bold instead of
uppercase, as it simply makes it easier to read.

If not otherwise specified, `git` repositories live on the
[GitLab.com](https://gitlab.com), in the [Prologin
group](https://gitlab.com/prologin), and will be referenced with their
subpath, most time with a link to the repository and a `git:` prefix. For
instance, <https://gitlab.com/prologin/tech/infra/infrastructure> will be referred
to as {{#git tech/infra/infrastructure}}.
