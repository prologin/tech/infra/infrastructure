# Git

The first tool you will need is `git`. Hopefully that is already installed on
your machine or you know how to install it. We use `git` repositories as we aim
for our infrastructure to be as code. The repository where most of what we do
happens is {{#git tech/infra/infrastructure}}.
Our repositories are organized as follows. Only the {{#git tech Tech group}} is
described as other groups are used for content-only repositories.

- [**Infrastructure/**](https://gitlab.com/prologin/tech/infra): our infrastructure!
  - [**infrastructure**](https://gitlab.com/prologin/tech/infra/infrastructure):
    as we said, this is where most of the work happens. We will come back to
    this repository [later on](./infrastructure-repository.md), as it is quite
    important.
  - [**Terraform/**](https://gitlab.com/prologin/tech/infra/terraform):
    Terraform states, modules and providers. Modules and providers are
    extracted from our main repository for the same reason as our Ansible
    repositories. Modules **must** be named `terraform-<main provider
    used>-modules`. Providers **must** be named
    `terraform-provider-<provider>`. Both must be mirrored to GitHub with the
    same nomenclature to be able to publish them on the Terraform Registry.
    States **should** live in our main infrastructure repository when possible.
    However, under some circumstances, for instance when specifying a list of
    our members as a resource, or when granting specific rights to members
    other than roots, they can be extracted to their own repository. There is
    no specific nomenclature for those.
- [**Packages/**](https://gitlab.com/prologin/tech/packages): these are projects
  that are meant to be distributed as packages, such as Python packages. They
  are mostly used by other projects.
- [**Services/**](https://gitlab.com/prologin/tech/services): these are projects
  that are meant to be distributed as services, and are not strictly related to
  a specific department of Prologin (for instance Concours or GCC!) such as
  some of our internal tools.
- [**Docker/**](https://gitlab.com/prologin/tech/docker): Docker images for
  packages, services and tools not developed by us. Docker images **should** be
  included with their project, so when the project is not ours, we create the
  Docker images we need in there.
- [**Tools/**](https://gitlab.com/prologin/tech/tools): various tools for our
  infrastructure and operations.

### Git configuration

The default branch of a repository **must** be `main`.

We always rebase our work, so setting `pull.rebase` to `true` might be a good
idea. You should also configure Git to GPG sign your commits. More on that
[later](./ssh.md).

### Archives

When archiving a repository, the archive feature of GitLab **must** be used
and there shall not be a top-level `archives/` group. This is done
automatically when removing it from our {{#git tech/infra/terraform/gitlab}}
repository.

### Notice about public repositories

Most of our repositories are public. Care should be taken when contributing to
those. Obviously, when pushing code, no secrets should ever be pushed. However,
contributions are not limited to code. Issues, merge requests, comments, etc.
also count as contributing. As such, always make sure that those are
well-written and actually contribute to the discussion.
