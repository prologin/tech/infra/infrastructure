# Quickstart

## With Nix

Some of our repositories now have a `flake.nix` file. If not, you're welcome to
open a merge request to add one. You will need to [install
Nix](https://nixos.org/manual/nix/stable/installation/installing-binary.html).
We recommend the multi-user installation, it will make your life much easier.
If you already are on NixOS, no need to install anything.

You then must configure Nix to recognize flakes. You can have a look at the
NixOS Wiki [page about flakes](https://nixos.wiki/wiki/Flakes) for a guide on
how to do that.

Again, some of our repositories include a `.envrc` file, that can interact with
[direnv](https://direnv.net/) and setup our Nix shell as it should be. Just
`direnv allow` and you're set. Otherwise, run `nix develop` and you're good to
go.

## Without Nix

Most of our repositories require Python tools. When such is the case, we use
[Poetry](https://python-poetry.org/) to manage dependencies. Install Poetry,
and then run `poetry install` and `poetry shell`, and all Python dependencies
will be in your `$PATH`.

You probably will have to install other tools as well. We recommend you look
inside the `flake.nix` files, under `devShell`, there should be a `buildInputs`
list that contains all required packages to work on the project.

## For both setups

There is some software that is not handled in our Poetry and Nix environments,
either because it requires system-level setup or because it requires one-time
manual setup.

### Docker

You will need to install Docker and docker-compose. Please refer to your
distribution documentation on how to install those.

### pre-commit

[pre-commit](https://pre-commit.com) should be installed by Poetry, but you
still must include it in your Git hooks: `pre-commit install`. Most of our
repositories have one, and it will prevent you from doing silly lint mistakes,
and save you time. Use it without moderation. Note however, that pre-commit
hooks shouldn't take forever, otherwise no-one would install them as it would
be too much of a hassle to make a commit.

### Kerberos

You probably want to install Kerberos, as it takes only a minute and will help
you in your day-to-day tasks. Please refer to your distribution documentation
on how to install it. For NixOS, you can reuse the [NixPIE
module](https://gitlab.cri.epita.fr/cri/infrastructure/nixpie/-/blob/master/modules/config/krb5.nix).
Some additional configuration is required in order to perform administrative
tasks. In `/etc/krb5.conf`, setup the following:

```
[libdefaults]
  default_realm = PROLOGIN.ORG
  dns_canonicalize_hostname = false
  dns_fallback = true
  rnds = false

[realms]
  PROLOGIN.ORG = {
    admin_server = kerberos.prologin.org
  }
```
