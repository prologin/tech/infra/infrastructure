# SSH

You may have a Yubikey. If not, we recommend you get one (at your own expense
however, as such it is not a hard requirement). We recommend you configure it
by following [drduh's guide](https://github.com/drduh/YubiKey-Guide). You can
use ed25519 keys instead of RSA if you want to. As the guide states, your
master keys **must** never be in contact with any form of network. You
**should** back them up in multiple places.

Here is a recommended SSH configuration:

```properties
# The main entrypoint for the infrastructure, our server hosted in a
# datacenter. We use it to ProxyJump to virtual machines that to not have a
# public IP. You can also use it to ProxyJump to machines that are running in
# our offices, however that configuration is not included here.
Host iris
  Hostname iris.th2.hxg.prologin.dev
  User root

# Always ProxyJump on the main router, overridden later on for other machines
Host *.prologin.dev
  ProxyJump iris
  User root

# Router for our KB Voltaire office
Host unicorn
  Hostname unicorn.kb3.epi.prologin.dev
  User root

# Other machines in our KB Voltaire office
Host *.kb3.epi.prologin.dev
  ProxyJump unicorn
  User root

# Some sane defaults, see below.
Host *
  ForwardAgent no
  ControlMaster yes
  ControlPath ~/.ssh/master-%r@%n:%p
  ControlPersist yes
  PubkeyAuthentication yes
```

Under `Host *`, add whatever is [currently recommended as modern
configuration](https://infosec.mozilla.org/guidelines/openssh#openssh-client)
by Mozilla. `ControlMaster yes` will keep your connections open even if you
disconnect from the server, which is quite useful when running tools such as
Ansible or `for i in $(seq 0 9); do ssh machine-$i do_something; done`[^kitty].
Also notice that `ForwardAgent` is set to `no`, so you will have to add `-A` to
your SSH command whenever you actually want to forward your agent. Feel free to
change any of those recommended defaults, but be aware of the potential
security implications.

A useful tool is SSH's proxy SOCKS. You can forward some of your traffic
through your SSH connection and it will appear as if you were using the network
of that machine. You can use it with `-D` and a port number (example: `ssh -D
5050`) and by configuring your browser to use that. It will allow you to,
amongst other things, access internal, non-publicly exposed services.

[^kitty]: every time you do this, somewhere, a kitten dies.
