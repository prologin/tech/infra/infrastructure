# Poetry

If you haven't read the [quickstart](./quickstart.md), go there now, everything
required to setup your Poetry environment is there.

We generally try to keep up with the latest version of Poetry, though we
currently do not have a specific requirement. Whatever is most up-to-date is
best.

We use [Renovate](https://github.com/renovatebot/renovate) to keep our
dependencies up-to-date, that is also the case for Poetry. The bot will open
merge requests whenever something should be updated in the `pyproject.toml`
file. It will also try to `poetry lock` once a week, on Monday before 05:00, to
update the lock file.

We use [black](https://github.com/psf/black) and
[isort](https://pycqa.github.io/isort/) to format our Python code. A pre-commit
hook **must** be included in repositories with Python code.
