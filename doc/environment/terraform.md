# Terraform

If you haven't read the [quickstart](./quickstart.md), go there now, everything
required to setup your Terraform environment is there.

We generally try to keep up with the latest version of Terraform, though we
currently do not have a specific requirement. Whatever is most up-to-date is
best.

We use [Renovate](https://github.com/renovatebot/renovate) to keep our
dependencies up-to-date, that is also the case for Terraform. The bot will open
merge requests whenever something should be updated.

We use the included `terraform fmt` to format our Terraform code. A pre-commit
hook **must** be included in repositories with Terraform code.

Some of our Terraform environments depend on [Vault](./vault.md) being
configured correctly.
