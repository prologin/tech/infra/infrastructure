# Environment

In this chapter, we will discuss what tools you must have installed on your
computer to properly work on the Prologin infrastructure. Please note that we
won't cover anything that is not directly related to the infrastructure, such
as your mail client configuration.
