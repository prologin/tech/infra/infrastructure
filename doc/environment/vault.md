# Vault

We use [HashiCorp Vault](https://www.vaultproject.io) to manage our secrets.
Make sure you have the CLI installed. We recommend you install it globally, as
it will make your daily life easier.

Check you can log in in [our Vault](https://vault.prologin.org).

The Vault CLI then needs to know which vault to use. You tell that by settings
the environment variable `VAULT_ADDR='https://vault.prologin.org:443'`. Adding
the port number makes for faster access, otherwise Vault will first try its
default port (8200) before falling back to the default HTTPS port. This is done
automatically when using the provided environment in
{{#git tech/infra/infrastructure}} as explained on [its dedicated
page](./infrastructure-repository.md).

TODO: how to login locally
