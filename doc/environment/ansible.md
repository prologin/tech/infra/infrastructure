# Ansible

We use [Poetry](./poetry.md) to manage our Ansible version.

Our Ansible roles are stored in {{#git tech/infra/ansible}}.
There is one repository per role, and a {{#git tech/infra/ansible/role-template}}
repository used to get some basic configuration when creating a new role
repository. This was done in order for anyone to use those, as it's not (at the
time of this writing) possible for Ansible to look into a sub-folder of another
repository to fetch a role. Role repositories **must** be named `role-<role>`
and **may** be mirrored to GitHub with the name `ansible-role-<role>`.

Our Ansible collections are also stored in {{#git tech/infra/ansible}}.
They are published as GitLab packages and are fetched from the
`requirements.yml` file using a direct link, as there is (at the time of this
writing) no other way of doing it. Collection repositories **must** be named
`collection-<collection>` and **may** be mirrored to GitHub with the name
`ansible-collection-<collection>`.

We use [ansible-lint](https://github.com/ansible-community/ansible-lint) to
lint our Ansible resources to ensure the best quality. Some of them also have
[molecule tests](https://github.com/ansible-community/molecule).

We also use [yamllint](https://github.com/adrienverge/yamllint) to lint our
YAML files. A pre-commit hook **must** be included in repositories with YAML
code.
