#!/usr/bin/env python3
"""
mdBook preprocessor

Replace strings matching `{{#git <repo> [optional display]}}` by an hyperlink to the repo, according to the specified configuration key `git-base-url`.

Example (with `git-base-url` being "https://gitlab.com/prologin/"):
{{#git gcc/site}} -> [`git:gcc/site`](https://gitlab.com/prologin/gcc/site)
{{#git gcc/site Site GCC!}} -> [Site GCC!](https://gitlab.com/prologin/gcc/site)
"""

import json
import re
import sys
from pprint import pprint

CONFIG = {}


def gitlink(matchobj):
    global CONFIG

    repo = matchobj.group("repo")
    display = matchobj.group("display")

    if not display:
        display = f"`git:{repo}`"
    return f"[{display}]({CONFIG['git-base-url']}{repo})"


def getlink_recurse_section(section):
    if chapter := section.get("Chapter"):
        chapter["content"] = GITLINK_RE.sub(gitlink, chapter["content"])

        for sub_item in chapter["sub_items"]:
            getlink_recurse_section(sub_item)


if __name__ == "__main__":
    if len(sys.argv) > 1:  # we check if we received any argument
        if sys.argv[1] == "supports":
            sys.exit(0)

    context, book = json.load(sys.stdin)

    CONFIG = context["config"]["preprocessor"]["gitlinks"]
    GITLINK_RE = re.compile(r"{{#git (?P<repo>[\w/_-]+)(?: (?P<display>.*))?}}")

    for section in book["sections"]:
        getlink_recurse_section(section)

    print(json.dumps(book))
