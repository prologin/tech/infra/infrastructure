# Summary

- [Preface](preface/introduction.md)
  - [Notes about this book](preface/notes.md)
- [Environment](environment/index.md)
  - [Quickstart](environment/quickstart.md)
  - [Git](environment/git.md)
  - [SSH](environment/ssh.md)
  - [Poetry](environment/poetry.md)
  - [Terraform](environment/terraform.md)
  - [Vault](environment/vault.md)
  - [Infrastructure repository](environment/infrastructure-repository.md)
