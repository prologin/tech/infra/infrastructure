locals {
  repositories = {
    "auth.th2.hxg.prologin.dev" = [
      "user_root",
    ]

    "camisole.th2.hxg.prologin.dev" = [
      "user_root",
    ]

    "gitlab-runner.th2.hxg.prologin.dev" = [
      "user_root",
    ]

    "iris.th2.hxg.prologin.dev" = [
      "user_root",
    ]

    "postgresql.th2.hxg.prologin.dev" = [
      "postgresql",
      "user_root",
    ]

    "redis.th2.hxg.prologin.dev" = [
      "redis",
      "user_root",
    ]

    "s3.th2.hxg.prologin.dev" = [
      "cloudserver",
      "user_root",
    ]

    "secure-services.th2.hxg.prologin.dev" = [
      "acme",
      "user_root",
      "vault",
    ]

    "services.th2.hxg.prologin.dev" = [
      "acme",
      "traefik",
      "user_root",
    ]
  }

  repositories_computed = merge([
    for machine, repositories in local.repositories : {
      for repository in repositories :
      "${machine}_${repository}" => {
        machine    = machine
        repository = repository
      }
    }
  ]...)
}
