# This file is maintained automatically by "tofu init".
# Manual edits may be lost in future updates.

provider "registry.opentofu.org/aminueza/minio" {
  version     = "1.5.2"
  constraints = "1.5.2"
  hashes = [
    "h1:rAngD3TtfUD9ZohzFo/QJe+2ae+kTmAzn4N2n6ayOOo=",
    "zh:02d90c08a5d8efddb7388eb74dae1722823a9bc6e54fc9e7e63a711108d20d58",
    "zh:03e541c770dc7785502ab6080e62328475a1af147957b3f2761750bf4c6cbf56",
    "zh:172b7538cef2ba2ccd79b5a84dcd27a88fbbe16f5fb1b9e3d2b6a05d63e17b29",
    "zh:3becdb228c2f78333265fec82abd16b1a14d86931cec96ff2726b5beafc290ac",
    "zh:4a0066767025a94b452fad7c0c58aa3103c1c521745b6c301ca64caa874501a2",
    "zh:4cd019edc99c6884d755f0f35122cb18bdd50ffab3a0690ecde139d2224d849f",
    "zh:5430f4d3cf757205d6b2ea82640475d0b164c61da9b1b944c3e66f829ed2eb82",
    "zh:882efeb742a3f1112034c05031b4ad686dda08ed7c6db6168339433b125288c4",
    "zh:99ad9e466ebabf83208a358230b871aa1c3576224a9112ee1038591a5e2154dc",
    "zh:9e06cefd61da232ca59c02e4a3d007c782c607af81f1640ef256ded367dd1730",
    "zh:c711cbd29a5d10411ee8510bf7ed49955af8d52c77bd3983f5f863595cf0c9c7",
    "zh:f7d7222bc796f362cf3b0211aca3ee256ac5a88cd52a1ddfbe918f01d8ed7970",
  ]
}

provider "registry.opentofu.org/hashicorp/aws" {
  version     = "4.52.0"
  constraints = "4.52.0"
  hashes = [
    "h1:/nnnCW3k4Z84TgdnkFwMLlWXqY3k3Ac7KdmnS2V5jbU=",
    "zh:438d3f671be41fc1e724b9bd295f247c704e2a1a01c93b2ded896911c84b2401",
    "zh:4cb8cfabc720c54b8c5c97dcc79fd87399a859f51a860cf5673d62e3476a0b54",
    "zh:647ee988a871d099b6ea51a46c20eb8a653d7ee4c42167c2e101a8442ded64c3",
    "zh:8661d5bfd64e76892a8a1a408c02cce8b07483163dbcf40d5b1c16a179f6337e",
    "zh:a6a1db583f1ad63dafa4908c619d2bc08528be7ada3ce491656c4c62b87649cc",
    "zh:aa7ca15c822e066b9ee635a4e97636fd739d44a42b050c181671d0378b863ede",
    "zh:b0551b846244202a87b121d7d83e13a307a5056b1497881b79bc5d4e13f29e23",
    "zh:b5646989d19ae91976a27d7e812d969298e4e370bc1eb8f6348c9fd96c384e62",
    "zh:f39cb220006816e61eae548fcfeb8da8a65a0a9f8a119dd6b6bb57b696a566b8",
    "zh:fb5093246c4f200dc485999cc9ebad69727fe56cbcb9793391e84cca003c506b",
  ]
}

provider "registry.opentofu.org/hashicorp/random" {
  version     = "3.4.3"
  constraints = "3.4.3"
  hashes = [
    "h1:57xIMCTAE78wv9naPFb3atFFEFn3rW1hOFYTrXMk/C0=",
    "zh:40f2ab718f177b0f8ec29da906104583047531de32cd7dc7f005a606a099d474",
    "zh:6a6684084bd1624b93a262663c5849f9efb597c99d6b03eeb6dbd685760561fb",
    "zh:8dc1973537166b468c08526ef38fa353f389df4ae9639cf8591dbc6e6048336b",
    "zh:aa260ea7793988e7f45ea3916ed6e177f8827e9dd3959fe799cbeda2329e7d23",
    "zh:b59bbf92b7be796c1921acf22b40fbb5e699bd3e9bdc06fa27a7e273509e1028",
    "zh:c4603614ca8c73f3c9b00cb4e89d3b859a62864cf09faba2ae376689a354f326",
    "zh:d82ba8432161763525dc7e8f32ac2377ea444829f805511cb90369b147c62b0c",
    "zh:ee058d8839b35e1dbcfea224652e6e921e015d3454e0c06e8afce3516bb50910",
    "zh:efffc2adfbfdbfde4f7fc9f423338c5969054de064b7afcd391fa2c419da2bc2",
    "zh:f0530bdae9985a3be630679d6c29396721616148a08594bb0a55551a69c53c13",
  ]
}

provider "registry.opentofu.org/hashicorp/vault" {
  version     = "3.12.0"
  constraints = "3.12.0"
  hashes = [
    "h1:3eN2Lod82+phqq3+Lw7X5QLU2yruHjT9joTrD9cB6g4=",
    "zh:07c6dcfd93ace5f357aaeb02b53a6838e4471409c712bfeff5f747e7ec896445",
    "zh:16cb2adee826377f9826af6b721dd93e9209e1128b20ea0461b2b658f6b24dfa",
    "zh:2009ee2bcfab634d1905b9fe304c2014bf4c5003d6723700ddbc808b6b5a37d0",
    "zh:3181c9f6939d0f50d6f334420795b2d7b4b40f4c7d5d80d657f1c1aaddbf5be4",
    "zh:4516a40d49868e6ddf83b551342c6f288ffa60199bfb72f149dfb8e000004777",
    "zh:8b1ef18b46895e3f234fee851b151bc995e0723d2c6cfed55e1febc659f5390f",
    "zh:9c4d466c85b0a7d7f7479a5f8f0d2fef81b78c9c1e4f405d49dfa1e5d90515bb",
    "zh:a0d7e6cff652bf1143db8469d74ee37fab8171a74111697ab07587fd5477d640",
    "zh:e175d3c1847ae1a4b572bbda322e7ac9c6957f67bb744976b385e5eda7734d6a",
    "zh:fbf59ec5d3dbbd5d0484971e4f0d6a39a5db176553ec7fcb700f442775c5e698",
  ]
}
