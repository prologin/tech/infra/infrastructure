---
libvirt_host_extra_daemon_packages:
  - libvirt-daemon-driver-storage-zfs

libvirt_host_pools:
  - name: images
    type: dir
    path: /var/lib/libvirt/images
    mode: 755
    owner: root
    group: root
  - name: vms
    type: zfs
    source: rpool/vms

libvirt_vms:
  - state: present
    name: gitlab-runner
    vcpus: 16
    memory_mb: 8192
    enable_spice: true
    volumes:
      - type: block
        format: raw
        dev: /dev/zvol/rpool/vms/gitlab-runner/os
    interfaces:
      - type: bridge
        source:
          dev: br-vms
        mac: 52:54:00:06:78:01
    boot_firmware: efi
    start: true
    autostart: true

  - state: present
    name: redis
    vcpus: 4
    memory_mb: 2048
    enable_spice: true
    volumes:
      - type: block
        format: raw
        dev: /dev/zvol/rpool/vms/redis/os
      - type: block
        format: raw
        dev: /dev/zvol/rpool/vms/redis/storage
    interfaces:
      - type: bridge
        source:
          dev: br-vms
        mac: 52:54:00:01:f3:d1
    boot_firmware: efi
    start: true
    autostart: true

  - state: present
    name: s3
    vcpus: 4
    memory_mb: 4096
    enable_spice: true
    volumes:
      - type: block
        format: raw
        dev: /dev/zvol/rpool/vms/s3/os
      - type: block
        format: raw
        dev: /dev/zvol/rpool/vms/s3/storage
    interfaces:
      - type: bridge
        source:
          dev: br-vms
        mac: 52:54:00:90:3b:66
    boot_firmware: efi
    start: true
    autostart: true

  - state: present
    name: secure-services
    vcpus: 2
    memory_mb: 3221
    enable_spice: true
    volumes:
      - type: block
        format: raw
        dev: /dev/zvol/rpool/vms/secure-services/os
      - type: block
        format: raw
        dev: /dev/zvol/rpool/vms/secure-services/storage
      - type: block
        format: raw
        dev: /dev/zvol/rpool/vms/secure-services/swap
    interfaces:
      - type: bridge
        source:
          dev: br-vms
        mac: 52:54:00:dd:7e:7b
    boot_firmware: efi
    start: true
    autostart: true

  - state: present
    name: postgresql
    vcpus: 8
    memory_mb: 12884
    enable_spice: true
    volumes:
      - type: block
        format: raw
        dev: /dev/zvol/rpool/vms/postgresql/os
      - type: block
        format: raw
        dev: /dev/zvol/rpool/vms/postgresql/storage
      - type: block
        format: raw
        dev: /dev/zvol/rpool/vms/postgresql/swap
    interfaces:
      - type: bridge
        source:
          dev: br-vms
        mac: 52:54:00:b0:ad:6d
    boot_firmware: efi
    start: true
    autostart: true

  - state: present
    name: services
    vcpus: 34
    memory_mb: 43008
    enable_spice: true
    volumes:
      - type: block
        format: raw
        dev: /dev/zvol/rpool/vms/services/os
      - type: block
        format: raw
        dev: /dev/zvol/rpool/vms/services/storage
      - type: block
        format: raw
        dev: /dev/zvol/rpool/vms/services/swap
    interfaces:
      - type: bridge
        source:
          dev: br-vms
        mac: 52:54:00:1f:f1:ce
    boot_firmware: efi
    start: true
    autostart: true

  - state: present
    name: auth
    vcpus: 2
    memory_mb: 2048
    enable_spice: true
    volumes:
      - type: block
        format: raw
        dev: /dev/zvol/rpool/vms/auth/os
      - type: block
        format: raw
        dev: /dev/zvol/rpool/vms/auth/storage
    interfaces:
      - type: bridge
        source:
          dev: br-vms
        mac: 52:54:00:19:71:4e
    boot_firmware: efi
    start: true
    autostart: true

  - state: present
    name: staging
    vcpus: 4
    memory_mb: 8192
    enable_spice: true
    volumes:
      - type: block
        format: raw
        dev: /dev/zvol/rpool/vms/staging/os
      - type: block
        format: raw
        dev: /dev/zvol/rpool/vms/staging/storage
    interfaces:
      - type: bridge
        source:
          dev: br-vms
        mac: 52:54:00:53:33:fe
    boot_firmware: efi
    start: true
    autostart: true

  - state: present
    name: userdir
    vcpus: 4
    memory_mb: 4096
    enable_spice: true
    volumes:
      - type: block
        format: raw
        dev: /dev/zvol/rpool/vms/userdir/os
      - type: block
        format: raw
        dev: /dev/zvol/rpool/vms/userdir/storage
    interfaces:
      - type: bridge
        source:
          dev: br-vms
        mac: 52:54:00:53:48:ca
    boot_firmware: efi
    start: true
    autostart: true

  - state: present
    name: semifinals
    vcpus: 4
    memory_mb: 10000
    enable_spice: true
    volumes:
      - type: block
        format: raw
        dev: /dev/zvol/rpool/vms/semifinals/os
    interfaces:
      - type: bridge
        source:
          dev: br-vms
        mac: 52:54:00:53:48:cb
      - type: bridge
        source:
          dev: br-ext-svc
        mac: 52:54:00:53:48:cd
    boot_firmware: efi
    start: true
    autostart: true

  - state: present
    name: extranet-services
    vcpus: 4
    memory_mb: 4096
    enable_spice: true
    volumes:
      - type: block
        format: raw
        dev: /dev/zvol/rpool/vms/extranet-services/os
      - type: block
        format: raw
        dev: /dev/zvol/rpool/vms/extranet-services/data
    interfaces:
      - type: bridge
        source:
          dev: br-vms
        mac: 52:54:00:46:0e:0f
      - type: bridge
        source:
          dev: br-ext-svc
        mac: 52:54:00:d6:cc:00
    boot_firmware: efi
    start: true
    autostart: false

  - state: present
    name: fw-extranet
    vcpus: 4
    memory_mb: 4096
    enable_spice: true
    volumes:
      - type: block
        format: raw
        dev: /dev/zvol/rpool/vms/fw-extranet/os
    interfaces:
      - type: bridge
        source:
          dev: br-vms
        mac: 52:54:00:ef:2d:25
      - type: bridge
        source:
          dev: br-ext-svc
        mac: 52:54:00:db:93:41
    boot_firmware: efi
    start: true
    autostart: false

  ### OLD VMS
  - state: present
    name: old-camisole
    vcpus: 8
    memory_mb: 8192
    enable_spice: true
    volumes:
      - pool: images
        format: qcow2
        name: camisole.qcow2
        capacity: 40G
    interfaces:
      - type: bridge
        source:
          dev: br-old-vms
        mac: 52:54:00:c8:39:a4
    boot_firmware: bios
    start: true
    autostart: true
  - state: present
    name: docker-services
    vcpus: 4
    memory_mb: 8192
    enable_spice: true
    volumes:
      - pool: images
        name: docker-services.qcow2
        format: qcow2
        capacity: 80G
    interfaces:
      - type: bridge
        source:
          dev: br-old-vms
        mac: 52:54:00:7d:32:17
    boot_firmware: efi
    start: true
    autostart: true
  - state: present
    name: rosa
    vcpus: 8
    memory_mb: 32768
    enable_spice: true
    volumes:
      - type: block
        format: raw
        dev: /dev/zvol/rpool/vms/rosa
    interfaces:
      - type: bridge
        source:
          dev: br-old-vms
        mac: 52:54:00:7d:32:18
    boot_firmware: bios
    start: true
    autostart: true
