# This file is maintained automatically by "tofu init".
# Manual edits may be lost in future updates.

provider "registry.opentofu.org/hashicorp/vault" {
  version     = "3.12.0"
  constraints = "3.12.0"
  hashes = [
    "h1:3eN2Lod82+phqq3+Lw7X5QLU2yruHjT9joTrD9cB6g4=",
    "zh:07c6dcfd93ace5f357aaeb02b53a6838e4471409c712bfeff5f747e7ec896445",
    "zh:16cb2adee826377f9826af6b721dd93e9209e1128b20ea0461b2b658f6b24dfa",
    "zh:2009ee2bcfab634d1905b9fe304c2014bf4c5003d6723700ddbc808b6b5a37d0",
    "zh:3181c9f6939d0f50d6f334420795b2d7b4b40f4c7d5d80d657f1c1aaddbf5be4",
    "zh:4516a40d49868e6ddf83b551342c6f288ffa60199bfb72f149dfb8e000004777",
    "zh:8b1ef18b46895e3f234fee851b151bc995e0723d2c6cfed55e1febc659f5390f",
    "zh:9c4d466c85b0a7d7f7479a5f8f0d2fef81b78c9c1e4f405d49dfa1e5d90515bb",
    "zh:a0d7e6cff652bf1143db8469d74ee37fab8171a74111697ab07587fd5477d640",
    "zh:e175d3c1847ae1a4b572bbda322e7ac9c6957f67bb744976b385e5eda7734d6a",
    "zh:fbf59ec5d3dbbd5d0484971e4f0d6a39a5db176553ec7fcb700f442775c5e698",
  ]
}
