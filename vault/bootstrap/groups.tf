locals {
  google_groups_policies = {
    "root@prologin.org" = [vault_policy.admin.name]
  }
}

resource "vault_identity_group" "google-groups" {
  for_each = local.google_groups_policies

  name     = each.key
  type     = "external"
  policies = each.value
}

resource "vault_identity_group_alias" "google-groups" {
  for_each = local.google_groups_policies

  name           = each.key
  mount_accessor = vault_jwt_auth_backend.google.accessor
  canonical_id   = vault_identity_group.google-groups[each.key].id
}
