resource "vault_policy" "admin" {
  name = "admin"

  policy = <<-EOF
    path "*" {
      capabilities = ["create", "read", "update", "patch", "delete", "list", "sudo"]
    }
  EOF
}
