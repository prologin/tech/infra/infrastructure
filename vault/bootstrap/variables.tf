variable "google_client_id" {
  type      = string
  sensitive = true
}

variable "google_client_secret" {
  type      = string
  sensitive = true
}

variable "google_credentials" {
  type    = string
  default = "secrets/google-sa.json"
}

variable "google_admin_email" {
  type      = string
  sensitive = true
}
