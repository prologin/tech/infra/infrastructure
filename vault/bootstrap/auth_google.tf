resource "vault_jwt_auth_backend" "google" {
  type = "oidc"
  path = "google"

  oidc_discovery_url = "https://accounts.google.com"
  oidc_client_id     = var.google_client_id
  oidc_client_secret = var.google_client_secret

  default_role = "default"

  provider_config = {
    provider                 = "gsuite"
    fetch_groups             = "true"
    fetch_user_info          = "true"
    groups_recurse_max_depth = "5"
    gsuite_admin_impersonate = var.google_admin_email
    gsuite_service_account   = sensitive(file(var.google_credentials))
  }

  tune {
    default_lease_ttl  = "768h"
    listing_visibility = "unauth"
    max_lease_ttl      = "768h"
    token_type         = "default-service"
  }
}

resource "vault_jwt_auth_backend_role" "default" {
  backend        = vault_jwt_auth_backend.google.path
  role_name      = "default"
  token_policies = ["default"]

  bound_audiences = ["${var.google_client_id}"]

  oidc_scopes  = ["profile", "email"]
  role_type    = "oidc"
  user_claim   = "email"
  groups_claim = "groups"

  claim_mappings = {
    sub   = "sub"
    email = "email"
    name  = "name"
  }

  allowed_redirect_uris = [
    "https://vault.prologin.dev/ui/vault/auth/google/oidc/callback",
    "https://vault.prologin.dev:8200/ui/vault/auth/google/oidc/callback",
    "http://localhost:8250/oidc/callback",
  ]
}
