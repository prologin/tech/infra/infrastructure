terraform {
  required_providers {
    vault = {
      source  = "hashicorp/vault"
      version = "3.12.0"
    }
  }

  backend "http" {}
}

provider "vault" {
  token = jsondecode(file("secrets/vault.json"))["root_token"]
}
