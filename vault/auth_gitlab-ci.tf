resource "vault_jwt_auth_backend" "gitlab-ci" {
  description        = "GitLab CI authentication"
  path               = "gitlab-ci"
  bound_issuer       = "https://gitlab.com"
  oidc_discovery_url = "https://gitlab.com"
}


# prologin/tech/infra/terraform/authentication-pie
resource "vault_policy" "gitlab-ci_prologin_tech_infra_terraform_authentication-pie" {
  name = "gitlab-ci_prologin_tech_infra_terraform_authentication-pie"

  policy = <<-EOF
    path "apps/data/freeipa/admin" {
      capabilities = ["read"]
    }
    path "authentik/data/admin" {
      capabilities = ["read"]
    }
  EOF
}
resource "vault_jwt_auth_backend_role" "gitlab-ci_prologin_tech_infra_terraform_authentication-pie" {
  backend        = vault_jwt_auth_backend.gitlab-ci.path
  role_name      = "gitlab-ci_prologin_tech_infra_terraform_authentication-pie"
  token_policies = [vault_policy.gitlab-ci_prologin_tech_infra_terraform_authentication-pie.name]

  role_type              = "jwt"
  user_claim             = "user_email"
  token_explicit_max_ttl = 5 * 60 # 5 minutes
  bound_audiences        = ["https://vault.prologin.dev"]
  bound_claims = {
    project_path = "prologin/tech/infra/terraform/authentication-pie"
  }
}

# prologin/tech/infra/terraform/eap-provisioner
resource "vault_policy" "gitlab-ci_prologin_tech_infra_terraform_eap-provisioner" {
  name = "gitlab-ci_prologin_tech_infra_terraform_eap-provisioner"

  policy = <<-EOF
    path "authentik/data/admin" {
      capabilities = ["read"]
    }
    path "apps/data/eap/providers/+" {
      capabilities = ["read"]
    }
  EOF
}
resource "vault_jwt_auth_backend_role" "gitlab-ci_prologin_tech_infra_terraform_eap-provisioner" {
  backend        = vault_jwt_auth_backend.gitlab-ci.path
  role_name      = "gitlab-ci_prologin_tech_infra_terraform_eap-provisioner"
  token_policies = [vault_policy.gitlab-ci_prologin_tech_infra_terraform_eap-provisioner.name]

  role_type              = "jwt"
  user_claim             = "user_email"
  token_explicit_max_ttl = 5 * 60 # 5 minutes
  bound_audiences        = ["https://vault.prologin.dev"]
  bound_claims = {
    project_path = "prologin/tech/infra/terraform/eap-provisioner"
  }
}
