resource "random_password" "apps_gcc_site_secret-key" {
  length  = 64
  special = false
}

resource "vault_generic_secret" "apps_gcc_site_secret-key" {
  path = "${vault_mount.apps.path}/gcc-site/secret-key"
  data_json = jsonencode({
    key = random_password.apps_gcc_site_secret-key.result
  })
}
