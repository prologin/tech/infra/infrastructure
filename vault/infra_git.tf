resource "vault_generic_secret" "infra_git_deploy-tokens" {
  for_each = toset([
    "asso-documents-legacy",
    "concours-archives",
    "concours-problems",
  ])

  path         = "${vault_mount.infra.path}/git/deploy-tokens/${each.key}"
  disable_read = true
  data_json = jsonencode({
    username = "FIXME"
    password = "FIXME"
  })
}
