resource "random_password" "apps_ndf_secret-key" {
  length  = 64
  special = false
}

resource "vault_generic_secret" "apps_ndf_secret-key" {
  path = "${vault_mount.apps.path}/ndf/secret-key"
  data_json = jsonencode({
    key = random_password.apps_ndf_secret-key.result
  })
}
