resource "random_password" "apps_ldap_admin" {
  length  = 64
  special = false
}

resource "vault_generic_secret" "apps_ldap_admin" {
  path = "${vault_mount.apps.path}/auth/ldap"
  data_json = jsonencode({
    password        = random_password.apps_ldap_admin.result
    password_hashed = "FIXME: mkpasswd -m sha512crypt"
  })
}

resource "random_password" "apps_database_password" {
  length  = 64
  special = false
}

resource "vault_generic_secret" "apps_database_password" {
  path = "${vault_mount.apps.path}/auth/kerberos"
  data_json = jsonencode({
    password = random_password.apps_database_password.result
  })
}
