resource "vault_mount" "acme" {
  path = "acme"
  type = "kv-v2"
}
resource "vault_generic_secret" "acme_cloudflare-token" {
  path         = "${vault_mount.acme.path}/cloudflare-token"
  disable_read = true
  data_json = jsonencode({
    token = "FIXME"
  })
}

resource "vault_auth_backend" "acme-approle" {
  path = "acme-approle"
  type = "approle"
}

resource "vault_policy" "acme_secure-services_th2_hxg_prologin_dev" {
  name = "acme_secure-services.th2.hxg.prologin.dev"

  policy = <<-EOF
    path "acme/data/certs/*" {
       capabilities = ["create", "read", "update", "list"]
    }
    path "acme/metadata/certs/*" {
       capabilities = ["create", "read", "update", "list"]
    }
  EOF
}
resource "vault_approle_auth_backend_role" "acme_secure-services_th2_hxg_prologin_dev" {
  backend        = vault_auth_backend.acme-approle.path
  role_name      = "acme_secure-services.th2.hxg.prologin.dev"
  token_policies = [vault_policy.acme_secure-services_th2_hxg_prologin_dev.name]
}
resource "vault_approle_auth_backend_role_secret_id" "acme_secure-services_th2_hxg_prologin_dev" {
  backend   = vault_auth_backend.acme-approle.path
  role_name = vault_approle_auth_backend_role.acme_secure-services_th2_hxg_prologin_dev.role_name
}
resource "vault_generic_secret" "acme_server-creds_secure-services_th2_hxg_prologin_dev" {
  path = "acme/server-creds/secure-services.th2.hxg.prologin.dev"
  data_json = jsonencode({
    role_id   = vault_approle_auth_backend_role.acme_secure-services_th2_hxg_prologin_dev.role_id
    secret_id = vault_approle_auth_backend_role_secret_id.acme_secure-services_th2_hxg_prologin_dev.secret_id
  })
}

resource "vault_policy" "acme_services_th2_hxg_prologin_dev" {
  name = "acme_services.th2.hxg.prologin.dev"

  policy = <<-EOF
    path "acme/data/certs/*" {
       capabilities = ["read"]
    }
    path "acme/metadata/certs/*" {
       capabilities = ["read"]
    }
  EOF
}
resource "vault_approle_auth_backend_role" "acme_services_th2_hxg_prologin_dev" {
  backend        = vault_auth_backend.acme-approle.path
  role_name      = "acme_services.th2.hxg.prologin.dev"
  token_policies = [vault_policy.acme_services_th2_hxg_prologin_dev.name]
}
resource "vault_approle_auth_backend_role_secret_id" "acme_services_th2_hxg_prologin_dev" {
  backend   = vault_auth_backend.acme-approle.path
  role_name = vault_approle_auth_backend_role.acme_services_th2_hxg_prologin_dev.role_name
}
resource "vault_generic_secret" "acme_server-creds_services_th2_hxg_prologin_dev" {
  path = "acme/server-creds/services.th2.hxg.prologin.dev"
  data_json = jsonencode({
    role_id   = vault_approle_auth_backend_role.acme_services_th2_hxg_prologin_dev.role_id
    secret_id = vault_approle_auth_backend_role_secret_id.acme_services_th2_hxg_prologin_dev.secret_id
  })
}

resource "vault_policy" "acme_postgresql_th2_hxg_prologin_dev" {
  name = "acme_postgresql.th2.hxg.prologin.dev"

  policy = <<-EOF
    path "acme/data/certs/postgresql.th2.hxg.prologin.dev" {
       capabilities = ["read"]
    }
    path "acme/metadata/certs/postgresql.th2.hxg.prologin.dev" {
       capabilities = ["read"]
    }
  EOF
}
resource "vault_approle_auth_backend_role" "acme_postgresql_th2_hxg_prologin_dev" {
  backend        = vault_auth_backend.acme-approle.path
  role_name      = "acme_postgresql.th2.hxg.prologin.dev"
  token_policies = [vault_policy.acme_postgresql_th2_hxg_prologin_dev.name]
}
resource "vault_approle_auth_backend_role_secret_id" "acme_postgresql_th2_hxg_prologin_dev" {
  backend   = vault_auth_backend.acme-approle.path
  role_name = vault_approle_auth_backend_role.acme_postgresql_th2_hxg_prologin_dev.role_name
}
resource "vault_generic_secret" "acme_server-creds_postgresql_th2_hxg_prologin_dev" {
  path = "acme/server-creds/postgresql.th2.hxg.prologin.dev"
  data_json = jsonencode({
    role_id   = vault_approle_auth_backend_role.acme_postgresql_th2_hxg_prologin_dev.role_id
    secret_id = vault_approle_auth_backend_role_secret_id.acme_postgresql_th2_hxg_prologin_dev.secret_id
  })
}

resource "vault_policy" "acme_auth_th2_hxg_prologin_dev" {
  name = "acme_auth.th2.hxg.prologin.dev"

  policy = <<-EOF
    path "acme/data/certs/ldap.prologin.org" {
       capabilities = ["read"]
    }
    path "acme/metadata/certs/ldap.prologin.org" {
       capabilities = ["read"]
    }
  EOF
}
resource "vault_approle_auth_backend_role" "acme_auth_th2_hxg_prologin_dev" {
  backend        = vault_auth_backend.acme-approle.path
  role_name      = "acme_auth.th2.hxg.prologin.dev"
  token_policies = [vault_policy.acme_auth_th2_hxg_prologin_dev.name]
}
resource "vault_approle_auth_backend_role_secret_id" "acme_auth_th2_hxg_prologin_dev" {
  backend   = vault_auth_backend.acme-approle.path
  role_name = vault_approle_auth_backend_role.acme_auth_th2_hxg_prologin_dev.role_name
}
resource "vault_generic_secret" "acme_server-creds_auth_th2_hxg_prologin_dev" {
  path = "acme/server-creds/auth.th2.hxg.prologin.dev"
  data_json = jsonencode({
    role_id   = vault_approle_auth_backend_role.acme_auth_th2_hxg_prologin_dev.role_id
    secret_id = vault_approle_auth_backend_role_secret_id.acme_auth_th2_hxg_prologin_dev.secret_id
  })
}
