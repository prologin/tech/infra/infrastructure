resource "vault_generic_secret" "infra_gitlab-runner_registration-tokens_prologin" {
  path         = "${vault_mount.infra.path}/gitlab-runner/registration-tokens/prologin"
  disable_read = true
  data_json = jsonencode({
    token = "FIXME"
  })
}
