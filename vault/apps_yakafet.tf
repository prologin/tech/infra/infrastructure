resource "random_password" "apps_yakafet_secret-key" {
  length  = 64
  special = false
}

resource "vault_generic_secret" "apps_yakafet_secret-key" {
  path = "${vault_mount.apps.path}/yakafet/secret-key"
  data_json = jsonencode({
    key = random_password.apps_yakafet_secret-key.result
  })
}
