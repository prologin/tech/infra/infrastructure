resource "vault_mount" "postgresql" {
  path = "postgresql"
  type = "kv-v2"
}

locals {
  postgresql_users = toset([
    "authentik",
    "gcc_site",
    "headscale",
    "ndf",
    "redmine",
    "vaultwarden",
    "yakafet",
  ])
}

resource "random_password" "postgresql_users" {
  for_each = local.postgresql_users
  length   = 64
  special  = false
}

resource "vault_generic_secret" "postgresql_users" {
  for_each = local.postgresql_users
  path     = "${vault_mount.postgresql.path}/users/${each.key}"
  data_json = jsonencode({
    password = random_password.postgresql_users[each.key].result
  })
}
