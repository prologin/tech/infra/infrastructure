locals {
  semifinals_instance_ids = [
    "343",
    "344",
    "345",
    "346",
    "347",
    "348",
    "349",
    "350",
    "351",
    "352",
    "353",
    "354",
    "355",
    "356",
    "357",
    "358",
    "359",
  ]
}

resource "random_password" "apps_semifinals_admin" {
  for_each = toset(local.semifinals_instance_ids)
  length   = 64
  special  = false
}

resource "random_password" "apps_semifinals_staff" {
  for_each = toset(local.semifinals_instance_ids)
  length   = 20
  special  = false
}

resource "random_password" "apps_semifinals_django_secret_key" {
  for_each = toset(local.semifinals_instance_ids)
  length   = 96
  special  = false
}

resource "vault_generic_secret" "apps_semifinals_local_accounts" {
  for_each = toset(local.semifinals_instance_ids)
  path     = "${vault_mount.apps.path}/semifinals/instances/${each.key}/local_accounts"
  data_json = jsonencode({
    admin = random_password.apps_semifinals_admin[each.key].result,
    staff = random_password.apps_semifinals_staff[each.key].result
  })
}

resource "vault_generic_secret" "apps_semifinals_basic_auth" {
  for_each = toset(local.semifinals_instance_ids)
  path     = "${vault_mount.apps.path}/semifinals/instances/${each.key}/basic_auth"
  data_json = jsonencode({
    admin = bcrypt(random_password.apps_semifinals_admin[each.key].result),
    staff = bcrypt(random_password.apps_semifinals_staff[each.key].result)
  })
}

resource "vault_generic_secret" "apps_semifinals_django" {
  for_each = toset(local.semifinals_instance_ids)
  path     = "${vault_mount.apps.path}/semifinals/instances/${each.key}/django"
  data_json = jsonencode({
    secret_key = random_password.apps_semifinals_django_secret_key[each.key].result
  })
}
