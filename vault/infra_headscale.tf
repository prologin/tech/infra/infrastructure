resource "vault_generic_secret" "infra_headscale_apikey" {
  path         = "${vault_mount.infra.path}/headscale/apikey"
  disable_read = true
  data_json = jsonencode({
    key = "FIXME: run headscale api create --expiration 365d"
  })
}

resource "vault_generic_secret" "infra_headscale_preauth-keys" {
  for_each     = toset(["th2.hxg", "kb1.ins"])
  path         = "${vault_mount.infra.path}/headscale/preauth-keys/${each.key}"
  disable_read = true
  data_json = jsonencode({
    key = "FIXME: create namespace ${each.key} and create a preauth key on https://vpn.prologin.org/web by connecting using headscale/apikey with a long expiry and make it reusable"
  })
}
