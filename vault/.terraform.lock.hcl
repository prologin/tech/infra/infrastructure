# This file is maintained automatically by "tofu init".
# Manual edits may be lost in future updates.

provider "registry.opentofu.org/hashicorp/random" {
  version     = "3.4.3"
  constraints = "3.4.3"
  hashes = [
    "h1:57xIMCTAE78wv9naPFb3atFFEFn3rW1hOFYTrXMk/C0=",
    "zh:40f2ab718f177b0f8ec29da906104583047531de32cd7dc7f005a606a099d474",
    "zh:6a6684084bd1624b93a262663c5849f9efb597c99d6b03eeb6dbd685760561fb",
    "zh:8dc1973537166b468c08526ef38fa353f389df4ae9639cf8591dbc6e6048336b",
    "zh:aa260ea7793988e7f45ea3916ed6e177f8827e9dd3959fe799cbeda2329e7d23",
    "zh:b59bbf92b7be796c1921acf22b40fbb5e699bd3e9bdc06fa27a7e273509e1028",
    "zh:c4603614ca8c73f3c9b00cb4e89d3b859a62864cf09faba2ae376689a354f326",
    "zh:d82ba8432161763525dc7e8f32ac2377ea444829f805511cb90369b147c62b0c",
    "zh:ee058d8839b35e1dbcfea224652e6e921e015d3454e0c06e8afce3516bb50910",
    "zh:efffc2adfbfdbfde4f7fc9f423338c5969054de064b7afcd391fa2c419da2bc2",
    "zh:f0530bdae9985a3be630679d6c29396721616148a08594bb0a55551a69c53c13",
  ]
}

provider "registry.opentofu.org/hashicorp/tls" {
  version     = "4.0.4"
  constraints = "4.0.4"
  hashes = [
    "h1:nCtGoZyE3/4/GJ7Gwto4NpVakR5QwMCgmKCWwtmZ1es=",
    "zh:21120c773efa9ef69424847669ef787892874d873d32b5feceb43c216232e524",
    "zh:2f8ef43f7a8981a007b632316032a16a67d052d1b58015dbd57318b405b3c35c",
    "zh:36fd3cabb9864951e986a761cc196ae2a09b0d2eb0b7718922c29aa4c0cdee88",
    "zh:42de2fad6e27f0936819e5454469cb2e8e5969d2fcfe7898c7daf05842935508",
    "zh:7a46748a20977307b520a0f1553034b7130b3f32ac2210366bcb252b9462244c",
    "zh:9a8c3df7b346d8193ab0969b07916483f5db326a45dd4e461f0e39ea7f3867c0",
    "zh:c02c979588787d617d10b3d6bcc74585de394c2b12c1d9511036f89566ce221c",
    "zh:c321a3db58ce89b92607821667ba95ced4eded42c8028586d0069c751c540a9a",
    "zh:c8ea6ff4d6245ff34dd2b2b52fe49f45e9dbd5972484dce953cd95f2b617d69f",
    "zh:d99659167401506a4ee778c6f956678763391610ccd4e2b6e1fc521c5488a256",
  ]
}

provider "registry.opentofu.org/hashicorp/vault" {
  version     = "3.12.0"
  constraints = "3.12.0"
  hashes = [
    "h1:3eN2Lod82+phqq3+Lw7X5QLU2yruHjT9joTrD9cB6g4=",
    "zh:07c6dcfd93ace5f357aaeb02b53a6838e4471409c712bfeff5f747e7ec896445",
    "zh:16cb2adee826377f9826af6b721dd93e9209e1128b20ea0461b2b658f6b24dfa",
    "zh:2009ee2bcfab634d1905b9fe304c2014bf4c5003d6723700ddbc808b6b5a37d0",
    "zh:3181c9f6939d0f50d6f334420795b2d7b4b40f4c7d5d80d657f1c1aaddbf5be4",
    "zh:4516a40d49868e6ddf83b551342c6f288ffa60199bfb72f149dfb8e000004777",
    "zh:8b1ef18b46895e3f234fee851b151bc995e0723d2c6cfed55e1febc659f5390f",
    "zh:9c4d466c85b0a7d7f7479a5f8f0d2fef81b78c9c1e4f405d49dfa1e5d90515bb",
    "zh:a0d7e6cff652bf1143db8469d74ee37fab8171a74111697ab07587fd5477d640",
    "zh:e175d3c1847ae1a4b572bbda322e7ac9c6957f67bb744976b385e5eda7734d6a",
    "zh:fbf59ec5d3dbbd5d0484971e4f0d6a39a5db176553ec7fcb700f442775c5e698",
  ]
}

provider "registry.opentofu.org/loafoe/htpasswd" {
  version     = "1.0.4"
  constraints = "1.0.4"
  hashes = [
    "h1:v/EZlkxlFBRlLIK2rmgbksuhbxOwenP3TQvreUhCAtE=",
    "zh:1f17ffcb8ab2f19de1242a6980f78334fc81efeaddfa85545435048f54045e4a",
    "zh:6265fd9bbb718d55655120044b4969c80aa938ecfb17a0fd7541ff7de8c54e1e",
    "zh:79b7a6e3260b084530f6bdaba13536843fa55fc28569965a69cbdcb5d5c208a5",
    "zh:827991bd4481b9c0f33a922f5168146d0e68f627c8c71f1c18da27df05386502",
    "zh:898a54254123718828d07ca54fba9626f6c706e4849c1d5bfd93d16df4463a6f",
    "zh:b42f93565c8e5ab902d12a44dc34efa7207f5a568c7588f957732be3d9cd3997",
    "zh:d43a78148ae10aac214c7abef7c131d78b7173d28ab679354ac67c11ff979f8e",
    "zh:ddb702db1b27df028dab0364fbd90a1f5d97244e41765a7e66a8afc1a85d8371",
    "zh:df22dd80e4639c14ec428d345cdf21851e807890cfe72908759d037cfaed68b7",
    "zh:f6c7dfbc72ad83727c1fcfc064adb0362d947b66a2f5ba185742d5668c598c96",
    "zh:f7e1feafd63a1987f5e39f9c75ac05dc153ffde2c9dd669847c19ad318bbebe7",
  ]
}
